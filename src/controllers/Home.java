package controllers;

import com.opensymphony.xwork2.ActionSupport;
import models.dao.AccountDAO;
import models.dao.GameStore;
import models.game.Game;
import org.apache.struts2.interceptor.validation.SkipValidation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tim on 12/12/14.
 */
public class Home extends ActionSupport
{
	private String username = AccountDAO.getActiveUsername();
	private Iterable<Game> unfinishedGames = GameStore.getPlayersUnfinishedGames(username);
	private Iterable<Game> openGames = GameStore.getOpenGames(username);
	private List<String> inputErrors;


	@SkipValidation //We know username exists
	public String execute()
	{
		if(inputErrors == null)
		{
			inputErrors = new ArrayList<>();
		}

		username = AccountDAO.getActiveUsername();
		unfinishedGames = GameStore.getPlayersUnfinishedGames(username);
		openGames = GameStore.getOpenGames(username);

		return SUCCESS;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public Iterable<Game> getUnfinishedGames()
	{
		return unfinishedGames;
	}

	public void setUnfinishedGames(Iterable<Game> unfinishedGames)
	{
		this.unfinishedGames = unfinishedGames;
	}

	public Iterable<Game> getOpenGames()
	{
		return openGames;
	}

	public void setOpenGames(Iterable<Game> openGames)
	{
		this.openGames = openGames;
	}

	public List<String> getInputErrors()
	{
		return inputErrors;
	}

	public void setInputErrors(List<String> inputErrors)
	{
		this.inputErrors = inputErrors;
	}
}
