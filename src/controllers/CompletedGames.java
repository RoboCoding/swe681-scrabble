package controllers;

import com.opensymphony.xwork2.ActionSupport;
import models.dao.AccountDAO;
import models.dao.GameStore;
import models.game.Game;
import models.view.CompletedGameViewModel;
import org.apache.shiro.SecurityUtils;

import java.util.Set;
import java.util.TreeSet;

/**
 * CompletedGames
 * Struts 2 Action class for viewing information about completed games.
 */
public class CompletedGames extends ActionSupport
{
	// The username of the logged-in user
	private String _username = AccountDAO.getActiveUsername();
	// The list of completed games involving the user
	private Set<CompletedGameViewModel> _completedGames = new TreeSet<>();

	/**
	 * Executes the action
	 * @return The result of the execution of the action
	 */
	public String execute()
	{
		if (!SecurityUtils.getSubject().isAuthenticated()) {
			return ERROR;
		}

		Iterable<String> playersWithCompletedGames = GameStore.getAllPlayersWithFinishedGame();

		for (String player : playersWithCompletedGames)
		{
			for(Game game : GameStore.getGamesByPlayer(player))
			{
				if(game.getIsGameOver())
				{
					_completedGames.add(new CompletedGameViewModel(game));
				}
			}
		}
		
		return SUCCESS;
	}

	/**
	 * Gets the logged-in username
	 * @return The name of the logged-in user
	 */
	public String getUsername()
	{
		return _username;
	}

	/**
	 * Sets the logged-in username
	 * @param username The name of the logged-in user
	 */
	public void setUsername(String username)
	{
		_username = username;
	}
	
	/**
	 * Gets the list of completed games
	 * @return The list of completed games
	 */
	public Iterable<CompletedGameViewModel> getCompletedGames()
	{
		return _completedGames;
	}
}
