package controllers;


import com.opensymphony.xwork2.ActionSupport;
import models.dao.AccountDAO;
import models.dao.GameStore;
import models.game.Board;
import models.game.Direction;
import models.game.Game;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class PlayMove extends ActionSupport
{
	private String username = AccountDAO.getActiveUsername();
	private int gameID;
	private Game storedGame;
	private String startingCoordinate;
	private String word;
	private String direction;
	private List<String> inputErrors = new ArrayList<>();
	private String pass;

	public String execute()
	{
		String moveString;
		storedGame = GameStore.importGame(gameID);

		if(pass!=null && pass.equals("pass"))
		{
			moveString = "PASS";
		}
		else
		{
			moveString = constructMoveCoding();
		}
		ArrayList<String> errors = new ArrayList<>();
		boolean valid = storedGame.processMove(moveString, errors);

		if(!valid)
		{
			for(String e : errors)
			{
				inputErrors.add(e);
			}
			return INPUT;
		}
		else
		{
			if(GameStore.exportGame(storedGame) == null)
			{
				inputErrors.add("The DB is busy. Please try again");
				return INPUT;
			}
			return SUCCESS;
		}

	}

	public void validate()
	{
		Pattern startingCoordinateValidation = Pattern.compile("^(([a-o][1-9])|([a-o]1[0-5]))$");
		if(pass !=null && pass.equalsIgnoreCase("pass") && startingCoordinate == null)
		{
			inputErrors.add("You must select a coordinate");
		}
		else if(pass !=null && pass.equalsIgnoreCase("pass") && startingCoordinate.trim().equals(""))
		{
			inputErrors.add("You must select a coordinate");
		}
		else if(pass!=null && !pass.equalsIgnoreCase("pass") && !startingCoordinateValidation.matcher
					(startingCoordinate).matches())
		{
			inputErrors.add("No such coordinate");
		}

		if(pass!=null && !pass.equalsIgnoreCase("pass") && getDirectionEnum()==null)
		{
			inputErrors.add("Direction was not set properly");
		}

		if(pass!=null && !pass.equalsIgnoreCase("pass") && word==null)
		{
			inputErrors.add("You must enter a word or pass your turn");
		}
		else if(pass!=null && !pass.equalsIgnoreCase("pass") && word.trim().equals(""))
		{
			inputErrors.add("You must enter a word or pass your turn");
		}

	}

	public Direction getDirectionEnum()
	{
		if(direction.equalsIgnoreCase("horizontal"))
		{
			return Direction.Horizontal;
		}
		else if(direction.equalsIgnoreCase("vertical"))
		{
			return Direction.Vertical;
		}
		else
		{
			return null;
		}
	}

	public String constructMoveCoding()
	{
		String coord;
		String coding;

		if(Direction.Horizontal == getDirectionEnum())
		{
			coord = startingCoordinate.substring(1, startingCoordinate.length());
			coord = coord + startingCoordinate.substring(0,1);
			coding = coord + " ";
		}
		else if(Direction.Vertical == getDirectionEnum())
		{
			coord = startingCoordinate;
			coding = coord + " ";
		}
		else
		{
			throw new IllegalStateException("Direction is not set");
		}
		return (coding + getLetterCoding()).toUpperCase();
	}

	private String getLetterCoding()
	{
		Board board = storedGame.getGameBoard();
		int x = getX();
		int y = Integer.parseInt(startingCoordinate.substring(1, startingCoordinate.length()));

		char[] chars = word.toCharArray();
		StringBuffer coding = new StringBuffer();
		if(Direction.Vertical == getDirectionEnum())
		{
			for(char c : chars)
			{
				if((board.getCharacterAt(x, y) != null) && board.getCharacterAt(x, y).toString().equalsIgnoreCase
						(Character.toString(c))) // Letter already on board.
				{
					coding.append("(");
					coding.append(c);
					coding.append(")");
				}
				else // Spot empty
				{
					coding = coding.append(c);
				}
				y++;
			}
		}
		else if(Direction.Horizontal == getDirectionEnum())
		{
			for(char c : chars)
			{
				if(board.getCharacterAt(x, y) != null && board.getCharacterAt(x,y).toString().equalsIgnoreCase
						(Character.toString(c))) // Letter already on board.
				{
					coding.append("(");
					coding.append(c);
					coding.append(")");
				}
				else // Spot empty
				{
					coding.append(c);
				}
				x++;
			}
		}
		else
		{
			throw new IllegalStateException("Direction is not set");
		}

		return coding.toString();

	}

	private int getX()
	{
		String xChar = startingCoordinate.substring(0, 1);
		int x = 0;

		switch(xChar){
			case "a" :
				x = 1;
				break;
			case "b" :
				x = 2;
				break;
			case "c" :
				x = 3;
				break;
			case "d" :
				x = 4;
				break;
			case "e" :
				x = 5;
				break;
			case "f" :
				x = 6;
				break;
			case "g" :
				x = 7;
				break;
			case "h" :
				x = 8;
				break;
			case "i" :
				x = 9;
				break;
			case "j" :
				x = 10;
				break;
			case "k" :
				x = 11;
				break;
			case "l" :
				x = 12;
				break;
			case "m" :
				x = 13;
				break;
			case "n" :
				x = 14;
				break;
			case "o" :
				x = 15;
				break;
		}

		return x;
	}

	public String getStartingCoordinate()
	{
		return startingCoordinate;
	}

	public void setStartingCoordinate(String startingCoordinate)
	{
		this.startingCoordinate = startingCoordinate;
	}

	public Game getStoredGame()
	{
		return storedGame;
	}

	public void setStoredGame(Game storedGame)
	{
		this.storedGame = storedGame;
	}

	public String getWord()
	{
		return word;
	}

	public void setWord(String word)
	{
		this.word = word;
	}

	public int getGameID()
	{
		return gameID;
	}

	public void setGameID(int gameID)
	{
		this.gameID = gameID;
	}

	public String getDirection()
	{
		return direction;
	}

	public void setDirection(String direction)
	{
		this.direction = direction;
	}

	public String getUsername()
	{
		return username;
	}

	public List<String> getInputErrors()
	{
		return inputErrors;
	}

	public void setInputErrors(List<String> errors)
	{
		this.inputErrors = errors;
	}

	public String getPass()
	{
		return pass;
	}

	public void setPass(String pass)
	{
		this.pass = pass;
	}
}
