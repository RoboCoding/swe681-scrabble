package controllers;

import com.opensymphony.xwork2.ActionSupport;
import models.dao.AccountDAO;
import models.dao.GameStore;
import models.game.Game;
import models.view.CompletedGameViewModel;
import org.apache.shiro.SecurityUtils;

/**
 * DetailedCompletedGame
 * Struts 2 Action class for viewing detailed game results
 */
public class DetailedCompletedGame extends ActionSupport
{
	// The username of the logged-in user
	private String _username = AccountDAO.getActiveUsername();
	// The ID of the game to retrieve
	private int _gameId;
	// The completed games involving the user to view details of
	private CompletedGameViewModel _detailedGame;

	/**
	 * Executes the action
	 * @return The result of the execution of the action
	 */
	public String execute()
	{
		if (!SecurityUtils.getSubject().isAuthenticated()) {
			return ERROR;
		}
		
		Game game = GameStore.importGame(_gameId);
		
		if (game == null) {
			return ERROR;
		}
		
		_detailedGame = new CompletedGameViewModel(game);
		return SUCCESS;
	}
	
	/**
	 * Gets the game ID
	 * @return The ID of this game.
	 */
	public int getGameId()
	{
		return _gameId;
	}
	
	/**
	 * Sets the game ID
	 * @param gameId The ID of this game.
	 */
	public void setGameId(int gameId)
	{
		_gameId = gameId;
	}

	/**
	 * Gets the logged-in username
	 * @return The name of the logged-in user
	 */
	public String getUsername()
	{
		return _username;
	}

	/**
	 * Sets the logged-in username
	 * @param username The name of the logged-in user
	 */
	public void setUsername(String username)
	{
		_username = username;
	}
	
	/**
	 * Gets the list of completed games
	 * @return The list of completed games
	 */
	public CompletedGameViewModel getDetailedGame()
	{
		return _detailedGame;
	}
}
