package controllers;

import com.opensymphony.xwork2.ActionSupport;
import models.dao.GameStore;
import models.game.Game;
import models.util.Stat;
import org.apache.shiro.SecurityUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * WinLossStats
 * Struts2 action class for view win-loss records.
 */
public class WinLossStats extends ActionSupport
{
	private List<Stat> stats;

	public String execute()
	{

		// "My Win-Loss Statistics"
		if (!SecurityUtils.getSubject().isAuthenticated()) {
			return ERROR;
		}

		stats = new ArrayList<>();
		Iterable<String> players = GameStore.getAllPlayersWithFinishedGame();

		for(String p : players)
		{
			Stat temp = new Stat();
			int tempGamesWon = 0;
			int tempGamesLost = 0;

			temp.setUsername(p);

			for (Game game : GameStore.getGamesByPlayer(p)) {
				if (game.getIsGameOver()) {
					if (game.getWinningPlayer().getPlayerLogin().equalsIgnoreCase(p)) {
						tempGamesWon++;
					} else {
						tempGamesLost++;
					}
				}
			}

			temp.setGamesWon(tempGamesWon);
			temp.setGamesLost(tempGamesLost);


			stats.add(temp);
		}


		return SUCCESS;
	}

	public List<Stat> getStats()
	{
		return stats;
	}

	public void setStats(List<Stat> stats)
	{
		this.stats = stats;
	}
}
