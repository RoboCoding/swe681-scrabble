package controllers;

import com.opensymphony.xwork2.ActionSupport;
import models.dao.AccountDAO;
import models.dao.GameStore;
import models.game.Game;

import java.util.ArrayList;
import java.util.List;

public class NewGame extends ActionSupport
{
	private String creator = AccountDAO.getActiveUsername();
	private int _gameID;
	private List<String> inputErrors;
	
	public String execute()
	{
		if(inputErrors == null )
		{
			inputErrors = new ArrayList<>();
		}
		// Check to see if user already has unstarted game.
		if(GameStore.hasUnstartedGame(creator))
		{
			inputErrors.add("Please wait for your unstarted game to start before creating a new game.");
			return INPUT;
		}
		else
		{
			Game newGame = new Game();
			if (!newGame.addPlayer(creator, inputErrors))
			{
				return INPUT;
			}

			newGame = GameStore.exportGame(newGame);
			if(newGame == null){
				inputErrors.add("The DB is busy. Please try again");
				return INPUT;
			}
			setGameID(newGame.getGameId());


		}
		return SUCCESS;
	}

	public void validate()
	{

	}

	public String getCreator()
	{
		return creator;
	}

	public int getGameID()
	{
		return _gameID;
	}
	
	public void setGameID(int gameID)
	{
		_gameID = gameID;
	}

	public List<String> getInputErrors()
	{
		return inputErrors;
	}

	public void setInputErrors(List<String> inputErrors)
	{
		this.inputErrors = inputErrors;
	}
}
