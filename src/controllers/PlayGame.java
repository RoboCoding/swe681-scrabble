package controllers;

import com.opensymphony.xwork2.ActionSupport;
import models.dao.AccountDAO;
import models.dao.GameStore;
import models.game.Game;
import models.view.PlayerGameViewModel;

import java.util.ArrayList;
import java.util.List;

public class PlayGame extends ActionSupport
{
	private String _username = AccountDAO.getActiveUsername();
	private int _gameID;
	private Game storedGame;
	private PlayerGameViewModel _gameViewModel;
	private List<String> inputErrors;

	public String getUsername()
	{
		return _username;
	}

	public int getGameID()
	{
		return _gameID;
	}
	
	public void setGameID(int gameID)
	{
		_gameID = gameID;
	}

	public PlayerGameViewModel getPlayerView()
	{
		return _gameViewModel;
	}

	// Display game board

	public String view()
	{
		if(inputErrors == null)
		{
			inputErrors = new ArrayList<>();
		}

		try {
			storedGame = GameStore.importGame(_gameID);
			_username = AccountDAO.getActiveUsername();

			_gameViewModel = new PlayerGameViewModel(storedGame, _username);

			//Time out
			if(storedGame.getIsGameStarted() && storedGame.timeOutSinceLastMove() && !storedGame.getIsGameOver())
			{
				storedGame.timeoutCurrentPlayer();
				if(GameStore.exportGame(storedGame) == null)
				{
					inputErrors.add("The DB is busy. Please try again");
					return INPUT;
				}
				return SUCCESS;

			}

			if(storedGame.getIsGameOver())
			{
				return SUCCESS;
			}


		} catch (Exception e) {
			inputErrors.add("There was an issue with the previous command. Please try again.");
			return INPUT;
		}
		return SUCCESS;
	}

	public List<String> getInputErrors()
	{
		return inputErrors;
	}

	public void setInputErrors(List<String> inputErrors)
	{
		this.inputErrors = inputErrors;
	}
}
