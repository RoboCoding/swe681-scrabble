package controllers;

import com.opensymphony.xwork2.ActionSupport;
import models.dao.AccountDAO;
import models.dao.GameStore;
import models.game.Game;
import models.game.Player;

import java.util.ArrayList;
import java.util.List;


public class JoinGame extends ActionSupport
{
	private String username = AccountDAO.getActiveUsername();
	private int gameID;
	private Game storedGame;
	private List<String> inputErrors;

	public String execute()
	{
		if(inputErrors == null)
		{
			inputErrors = new ArrayList<>();
		}

		// Try to import the gameID;
		storedGame = GameStore.importGame(gameID);
		if(storedGame == null)
		{
			inputErrors.add("No such game exists with that game ID");
			return INPUT;
		}

		//Check if player is already part of the game
		List<Player> players = storedGame.getAllPlayers();
		boolean alreadyInGame = false;
		for(Player p : players)
		{
			if(p.getPlayerLogin().equalsIgnoreCase(username))
			{
				alreadyInGame = true;
				break;
			}
		}

		if(!alreadyInGame)
		{
			//Try to add player to game
			storedGame.addPlayer(getUsername(), inputErrors);
		}

		if(inputErrors.isEmpty())
		{
			if(storedGame.getAllPlayers().size() >= 2) // 2 player Scrabble
			{
				storedGame.startGame(inputErrors);
			}

			if(!(GameStore.exportGame(storedGame) == null))
			{
				return SUCCESS;
			}
			else
			{
				inputErrors.add("The DB is busy. Please try again");
				return INPUT;
			}
		} else
		{
			return INPUT;
		}
	}

	public int getGameID()
	{
		return gameID;
	}

	public void setGameID(int gameID)
	{
		this.gameID = gameID;
	}

	public Game getStoredGame()
	{
		return storedGame;
	}

	public void setStoredGame(Game storedGame)
	{
		this.storedGame = storedGame;
	}

	public String getUsername()
	{
		return username;
	}

	public List<String> getInputErrors()
	{
		return inputErrors;
	}

	public void setInputErrors(List<String> inputErrors)
	{
		this.inputErrors = inputErrors;
	}
}
