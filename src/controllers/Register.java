package controllers;

import com.opensymphony.xwork2.ActionSupport;
import com.stormpath.sdk.resource.ResourceException;
import models.dao.AccountDAO;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.struts2.interceptor.ServletRequestAware;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;


/**
 * Controller for processing Registrations
 */
public class Register extends ActionSupport implements ServletRequestAware
{
	private String desiredUsername;
	private String desiredPassword;
	private String confirmDesiredPassword;
	private ServletRequest request;

	@Override
	public String execute()
	{
		try
		{
			AccountDAO.createAccount(desiredUsername, desiredPassword);
			AccountDAO.authenticateAccount(desiredUsername, desiredPassword, request.getRemoteHost());

			return SUCCESS;
		}catch (ResourceException | IllegalArgumentException | AuthenticationException e)
		{
			return ERROR;
		}
	}

	@Override
	public void validate()
	{
		Pattern regEx = AccountDAO.getUsernameValidation();
		Pattern passRegEx = AccountDAO.getPasswordValidation();

		if(desiredUsername == null || desiredUsername.trim().equals(""))
		{
			this.addFieldError("desiredUsername", "Username is required");
		}
		else if(desiredUsername.length() > 50)
		{
			addFieldError("desiredUsername", "Max characters is 50");
		}
		else if(!regEx.matcher(desiredUsername).matches())
		{
			addFieldError("desiredUsername", "Username must start with a letter: A-Z or a-z and can include letters, " +
			                                 "numbers and _!@#$%^&-* characters");
		}

		if(desiredPassword == null || desiredPassword.trim().equals(""))
		{
			addFieldError("desiredPassword", "Password is required");
		}
		else if(desiredPassword.length() > 50)
		{
			addFieldError("desiredPassword", "Max characters is 50");
		}
		else if(!passRegEx.matcher(desiredPassword).matches())
		{
			addFieldError("desiredPassword", "Password must contain at least 1 of each: [a-z] [A-Z] [0-9] \n Allowed " +
			                                 "characters: A-Z, a-z, 0-9, _!@#$%^&*");
		}
		else if(confirmDesiredPassword == null || desiredPassword.trim().equals(""))
		{
			addFieldError("confirmDesiredPassword", "You must confirm your password");
		}
		else if(!confirmDesiredPassword.equals(desiredPassword))
		{
			addFieldError("desiredPassword", "Passwords do not match");
		}
		else if(AccountDAO.usernameExists(desiredUsername))
		{
			addFieldError("desiredUsername", "Username already taken");
		}

	}

	public String getDesiredPassword()
	{
		return desiredPassword;
	}

	public void setDesiredPassword(String desiredPassword)
	{
		this.desiredPassword = desiredPassword;
	}

	public String getDesiredUsername()
	{
		return desiredUsername;
	}

	public void setDesiredUsername(String desiredUsername)
	{
		this.desiredUsername = desiredUsername;
	}

	public String getConfirmDesiredPassword()
	{
		return confirmDesiredPassword;
	}

	public void setConfirmDesiredPassword(String confirmDesiredPassword)
	{
		this.confirmDesiredPassword = confirmDesiredPassword;
	}

	public ServletRequest getServletRequest()
	{
		return request;
	}

	@Override
	public void setServletRequest(HttpServletRequest httpServletRequest)
	{
		request = httpServletRequest;
	}
}
