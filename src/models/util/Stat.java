package models.util;

public class Stat
{
	private int gamesWon;
	private int gamesLost;
	private String username;

	public int getGamesLost()
	{
		return gamesLost;
	}

	public void setGamesLost(int gamesLost)
	{
		this.gamesLost = gamesLost;
	}

	public int getGamesWon()
	{
		return gamesWon;
	}

	public void setGamesWon(int gamesWon)
	{
		this.gamesWon = gamesWon;
	}

	public int getWinPercentage()
	{
		if((gamesWon + gamesLost) <= 0)
		{
			return 100;
		}

		return (int)Math.round(((double) gamesWon) / (gamesWon + gamesLost) * 100);
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}
}
