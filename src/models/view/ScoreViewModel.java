/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models.view;

import models.game.Player;

/**
 *
 * @author NetworkTitan
 */
public class ScoreViewModel
{
	private String _playerName;
	private int _score;
	
	public ScoreViewModel() 
	{
	}
	
	public ScoreViewModel(Player player)
	{
		_playerName = player.getPlayerLogin();
		_score = player.getScore();
	}
	
	public String getPlayerName()
	{
		return _playerName;
	}
	
	public void setPlayerName(String playerName)
	{
		_playerName = playerName;
	}
	
	public int getScore()
	{
		return _score;
	}
	
	public void setScore(int score)
	{
		_score = score;
	}
}
