package models.view;

import models.game.Move;

/**
 * MoveViewModel
 * The model of a Move class to display in the view
 */
public class MoveViewModel
{
	// The name of the player
	private final String _player;
	// The text for this move
	private final String _moveText;
	// The score of this move
	private final int _moveScore;
	
	public MoveViewModel(Move move)
	{
		_player = move.getPlayerLogin();
		_moveText = move.getMoveCoding();
		_moveScore = move.getScore();
	}
	
	/**
	 * Gets the name of the player that made this move.
	 * @return String representing the login of the player that made this move.
	 */
	public String getPlayer()
	{
		return _player;
	}
	
	/**
	 * Gets the score for this move
	 * @return The score for this move.
	 */
	public int getScore()
	{
		return _moveScore;
	}
	
	/**
	 * Gets the text of this move.
	 * @return The text of this move.
	 */
	public String getMove()
	{
		return _moveText;
	}
}
