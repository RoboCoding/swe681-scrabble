package models.view;

import models.game.Player;

/**
 * PlayerResultViewModel
 * Model of the result of a game for a particular player
 */
public class PlayerResultViewModel
{
	// The name of this player
	private final String _name;
	// The score for this player
	private final int _score;
	
	/**
	 * Initializes a new instance of the PlayerResultViewModel class from the 
	 * specified Player.
	 * @param player The player to initialize this model with.
	 */
	public PlayerResultViewModel(Player player)
	{
		_name = player.getPlayerLogin();
		_score = player.getScore();
	}
	
	/**
	 * Gets the name of this player
	 * @return The String representing the name of this player.
	 */
	public String getPlayerName()
	{
		return _name;
	}
	
	/**
	 * Gets the score for this player
	 * @return The score of this player
	 */
	public int getPlayerScore()
	{
		return _score;
	}
}
