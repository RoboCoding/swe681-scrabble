package models.view;

import models.game.Game;
import models.game.Move;
import models.game.Player;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * ActiveGameViewModel
 * The model for representing an active game in the view
 */
public class CompletedGameViewModel implements Comparable<CompletedGameViewModel>
{
	// The ID of the completed game
	private final int _gameID;
	// The time stamp when this game started
	private final Date _gameStartTimestamp;
	// The player that won the game
	private String _winningPlayer;
	// The other players from this game
	private final List<PlayerResultViewModel> _players = new ArrayList<>();
	// The moves that took place in this game
	private final List<MoveViewModel> _moves = new ArrayList<>();
	
	public CompletedGameViewModel(Game game)
	{
		_gameID = game.getGameId();
		_winningPlayer = game.getWinningPlayer().getPlayerLogin();
		for (Player player : game.getAllPlayers()) {
			_players.add(new PlayerResultViewModel(player));
		}
		_gameStartTimestamp = game.getGameStartTimestamp();
		for (Move move : game.getAllMoves()) {
			_moves.add(new MoveViewModel(move));
		}
	}
	
	/**
	 * Gets the game ID
	 * @return The ID of the game
	 */
	public int getGameId()
	{
		return _gameID;
	}
	
	/**
	 * Gets the number of players from this game.
	 * @return The number of players in this game.
	 */
	public int getNumPlayers()
	{
		return _players.size();
	}

	/**
	 * Gets the winner of the game
	 * @return The name of the player that won the game
	 */
	public String getWinningPlayer()
	{
		return _winningPlayer;
	}
	
	/**
	 * Gets when this game started
	 * @return Date representing when this game started
	 */
	public Date getGameStartTimestamp()
	{
		return _gameStartTimestamp;
	}
	
	/**
	 * Gets records for all moves in this game
	 * @return The moves from this game
	 */
	public Iterable<MoveViewModel> getMoves()
	{
		return _moves;
	}
	
	/**
	 * Gets records for other players in the game
	 * @return The other players in the game
	 */
	public Iterable<PlayerResultViewModel> getPlayers()
	{
		return _players;
	}

	public int compareTo(CompletedGameViewModel obj)
	{
		int tempID = obj.getGameId();

		if(this._gameID < tempID)
		{
			return -1;
		}
		else if(this._gameID == tempID)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	@Override
	public boolean equals(Object obj)
	{
		if(!(obj instanceof CompletedGameViewModel))
		{
			return false;
		}
		else
		{
			return ((CompletedGameViewModel) obj).getGameId() == getGameId();
		}
	}

	@Override
	public int hashCode()
	{
		assert false : "hashCode not designed";
		return 42;
	}
}
