package models.view;

import models.game.*;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class PlayerGameViewModel
{
	private ScoreViewModel _currentPlayer;
	private final List<ScoreViewModel> _allPlayers = new ArrayList<>();
	private String _playerOnClock;
	private final List<Character> _tiles = new ArrayList<>();
	private boolean isGameStarted = false;
	private Iterable<Letter> playedLetters;
	private boolean isGameOver = false;
	private String winner;
	private int winningScore;

	public PlayerGameViewModel(){}
	
	public PlayerGameViewModel(Game game, String playerName)
	{
		for (Player player : game.getAllPlayers()) {
			ScoreViewModel playerModel = new ScoreViewModel(player);
			if (player.getPlayerLogin().equals(game.getCurrentPlayer().getPlayerLogin())) {
				_playerOnClock = player.getPlayerLogin();
			}
			String playerLogin = player.getPlayerLogin();
			if (playerLogin != null && playerLogin.equalsIgnoreCase(playerName)) {
				_currentPlayer = playerModel;
				setMyTiles(player.getLetters());
			}
			_allPlayers.add(playerModel);
		}

		isGameStarted = game.getIsGameStarted();
		isGameOver = game.getIsGameOver();
		playedLetters = getPlayedLetters(game);

		if(isGameOver)
		{
			winner = game.getWinningPlayer().getPlayerLogin();
			winningScore = game.getWinningPlayer().getScore();
		}



	}
	
	public boolean getIsMyTurn()
	{
		return _currentPlayer != null && _currentPlayer.getPlayerName().equalsIgnoreCase(_playerOnClock);
	}
	
	public Iterable<ScoreViewModel> getAllPlayers()
	{
		return _allPlayers;
	}
	
	public void setAllPlayers(Iterable<ScoreViewModel> players)
	{
		_allPlayers.clear();
		for (ScoreViewModel player : players) {
			_allPlayers.add(player);
		}
	}
	
	public String getPlayerOnClock()
	{
		return _playerOnClock;
	}
	
	public void setPlayerOnClock(String playerName)
	{
		_playerOnClock = playerName;
	}
	
	public Iterable<Character> getMyTiles()
	{
		return _tiles;
	}
	
	public final void setMyTiles(Iterable<Character> tiles)
	{
		_tiles.clear();
		for (Character tile : tiles) {
			_tiles.add(tile);
		}
	}

	public boolean getIsGameStarted()
	{
		return isGameStarted;
	}

	public void setIsGameStarted(boolean isGameStarted)
	{
		this.isGameStarted = isGameStarted;
	}

	public Iterable<Letter> getPlayedLetters()
	{
		return playedLetters;
	}

	private Iterable<Letter> getPlayedLetters(Game storedGame)
	{
		Word playedLetters = new Word();
		Board board = storedGame.getGameBoard();

		for(int i = 1; i <= 15; i++)
		{
			for(int j = 1; j<= 15; j++)
			{
				Character temp = board.getCharacterAt(i, j);
				if(temp != null)
				{
					playedLetters.addLetter(new Letter(i, j, temp, false));
				}
			}
		}

		return playedLetters;
	}

	public boolean getIsGameOver()
	{
		return isGameOver;
	}

	public String getWinner()
	{
		return winner;
	}

	public int getWinningScore()
	{
		return winningScore;
	}
}
