package models.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * LetterPouch Class representing the repository of letters for the Scrabble
 * Game.
 */
public class LetterPouch
{

	// The total number of letters in the pouch.

	private static final int LETTER_COUNT = 98;
	// The list of letters in the pouch.
	private final List<Character> _letterBag;
	// The next letter to be returned from the pouch.
	private int _nextLetterIndex = 0;
	// The random number generator for shuffling the letter pouch.
	private final Random _rng;

	/**
	 * Initializes a new instance of the LetterPouch class.
	 */
	public LetterPouch()
	{
		_rng = new Random();
		_letterBag = new ArrayList<>();
		initializeLetterPouch();
		Collections.shuffle(_letterBag, _rng);
	}

	/**
	 * Initializes a new instance of the LetterPouch class from the given
	 * letters.
	 *
	 * @param initializedPouch The letters and ordering with which to fill this.
	 */
	public LetterPouch(String initializedPouch)
	{
		_rng = new Random();
		_letterBag = new ArrayList<>();
		for (int i = 0; i < initializedPouch.length(); i++) {
			_letterBag.add(initializedPouch.charAt(i));
		}
	}

	/**
	 * Returns the number of tile yet to be returned in this pouch.
	 *
	 * @return An int representing the number of tiles not yet returned.
	 */
	public int getTilesRemaining()
	{
		return LETTER_COUNT - _nextLetterIndex;
	}

	/**
	 * Returns whether all tiles have been returned from this.
	 *
	 * @return True if all tiles have been returned from this. Else, false.
	 */
	public boolean getIsBagEmpty()
	{
		return getTilesRemaining() == 0;
	}

	/**
	 * Return up to the specified number of letters from this pouch. Only
	 * returns fewer letters if the bag empties entirely before.
	 *
	 * @param numLetters The number of letters to return.
	 * @return An iterable of characters representing the returned letters.
	 */
	public Iterable<Character> getLetters(int numLetters)
	{
		if (getIsBagEmpty()) {
			throw new IllegalStateException("LetterPouch is empty!");
		}

		List<Character> returnValue = new ArrayList<>();
		while (numLetters > 0 && getTilesRemaining() > 0) {
			returnValue.add(getNextLetter());
			numLetters--;
		}
		return returnValue;
	}

	/**
	 * Returns and replaces a random letter from this pouch. The letter is
	 * "returned" to the pouch and does not affect the remaning letters.
	 *
	 * @return A random character from this
	 */
	public Character getRandomLetter()
	{
		return _letterBag.get(_rng.nextInt(LETTER_COUNT));
	}
	
	/**
	 * Gets all letters from this pouch in order.
	 * @return A string representing all letters in this pouch in order.
	 */
	public String getAllContents()
	{
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < _letterBag.size(); i++) {
			builder.append(_letterBag.get(i));
		}
		return builder.toString();
	}

	/**
	 * Gets the next character from this pouch.
	 *
	 * @return The next character in this.
	 */
	private Character getNextLetter()
	{
		if (getIsBagEmpty()) {
			throw new IllegalStateException("LetterPouch is empty!");
		}
		return _letterBag.get(_nextLetterIndex++);
	}

	/**
	 * Initializes the state of this pouch to represent standard Scrabble rules.
	 */
	private void initializeLetterPouch()
	{
		for (int i = 1; i <= 12; i++) {
			if (i <= 1) {
				_letterBag.add('K');
				_letterBag.add('J');
				_letterBag.add('X');
				_letterBag.add('Q');
				_letterBag.add('Z');
			}

			if (i <= 2) {
				_letterBag.add('B');
				_letterBag.add('C');
				_letterBag.add('M');
				_letterBag.add('P');
				_letterBag.add('F');
				_letterBag.add('H');
				_letterBag.add('V');
				_letterBag.add('W');
				_letterBag.add('Y');
			}

			if (i <= 3) {
				_letterBag.add('G');
			}

			if (i <= 4) {
				_letterBag.add('L');
				_letterBag.add('S');
				_letterBag.add('U');
				_letterBag.add('D');
			}

			if (i <= 6) {
				_letterBag.add('N');
				_letterBag.add('R');
				_letterBag.add('T');
			}

			if (i <= 8) {
				_letterBag.add('O');
			}

			if (i <= 9) {
				_letterBag.add('A');
				_letterBag.add('I');
			}

			if (i <= 12) {
				_letterBag.add('E');
			}
		}
	}
}
