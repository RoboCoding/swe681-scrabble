package models.game;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 * Board
 * An abstraction of a Scrabble game board.
 */
public class Board 
{
    // The minimum x or y index at which a letter can be placed (1-indexed).
    public static final int MIN_INDEX = 1;
    // The maximum x or y index at which a letter can be placed (1-indexed).
    public static final int MAX_INDEX = 15;
    // The characters representing the board itself.
    private final Character _board[][] = new Character[MAX_INDEX][MAX_INDEX];
    
	/**
	 * Returns a copy of the characters on this board (useful for the view)
	 * @return A doubly-indexed array representing the letters on the board.
	 */
	public Character[][] getBoardContents()
	{
		Character newBoard[][] = new Character[MAX_INDEX][MAX_INDEX];
		for (int x = 0; x < MAX_INDEX; x++) {
			System.arraycopy(_board[x], 0, newBoard[x], 0, MAX_INDEX);
		}
		return newBoard;
	}
	
    /**
     * Gets the character from this board at the specified coordinates.
     * @param x The X-coordinate for the letter to return
     * @param y The Y-coordinate for the letter to return
     * @return The Character stored at the specified x- and y-coordinates. May 
     * return null if no Character is found.
     * @exception IllegalArgumentException Thrown if x or y are not valid 
     * coordinates on the game board.
     */
    public Character getCharacterAt(int x, int y) throws IllegalArgumentException
    {
        if (x < MIN_INDEX || x > MAX_INDEX) {
            throw new IllegalArgumentException("Invalid X");
        }
        if (y < MIN_INDEX || y > MAX_INDEX) {
            throw new IllegalArgumentException("Invalid Y");
        }
        return _board[x-1][y-1];
    }
    
    /**
     * Stores the given letter at the specified coordinates.
     * @param x The x-coordinate of the letter to same
     * @param y
     * @param characterToSave The character to assign to this board
     * @exception IllegalArgumentException Thrown if the given x or y 
     * coordinates are not valid for this game board or if the character to 
     * save is not alphabetic.
     */
    private void setCharacterAt(int x, int y, Character characterToSave)
    {
        if (!Character.isAlphabetic(characterToSave)) {
            throw new IllegalArgumentException("Character to save not alphabetic");
        }
        
        if (x < MIN_INDEX || x > MAX_INDEX) {
            throw new IllegalArgumentException("Invalid X");
        }
        if (y < MIN_INDEX || y > MAX_INDEX) {
            throw new IllegalArgumentException("Invalid Y");
        }
        _board[x-1][y-1] = characterToSave;
    }
    
    /**
     * Determines the scorable words that would be formed by applying the given 
     * Move to this board. Does not commit the move.
     * @param playerMove The move to examine relative to this board
     * @return A list of Word objects representing the words formed when 
     * applying the given Move to this board.
     */
    public List<Word> examineMove(Move playerMove)
    {
        Direction orientation = playerMove.getOrientation();
        List<Word> scoringWords = new ArrayList<>();
        
        // The word from the Move itself is always a scoring word.
        scoringWords.add(playerMove.getWord());
        
        for (Letter letter : playerMove.getWord()) {
            Point letterPos = letter.getLetterPosition();
            Character existingChar = getCharacterAt(letterPos.x, letterPos.y);
            
            if (!letter.getIsEligibleForBonuses()) {
                if (null == existingChar) {
                    // If a letter is not eligible for bonus, then there must 
                    // be a letter at that location on this board.
                    throw new IllegalArgumentException("Existing char not found where expected");
                } else if (!existingChar.equals(letter.getLetter())){
                    // Letter being played in Move must match existing letter.
                    throw new IllegalArgumentException("Existing char not matching what expected");
                }
            } else {
				if (existingChar != null) {
					// If a letter is eligible for bonus, then there must not 
					// be a letter at that location on the board.
					throw new IllegalArgumentException("Char found where one was not expected");
				}
                // Examine the side word for the current letter.
                if (hasSideWord(letter, orientation)) {
                    scoringWords.add(getSideWord(letter, orientation));
                }
            }
        }
        
        return scoringWords;
    }
	
	/**
	 * Gets a move that is equivalent to the existing move but uses more 
	 * continuous letters
	 * @param playerMove The move to expand
	 * @return Returns a Move object that is expanded to use all connected 
	 * letters. If the given move is already maximally expanded, the original 
	 * move is returned.
	 */
	public Move expandWord(Move playerMove)
	{
		Direction orientation = playerMove.getOrientation();
		
		int minX = 16, minY = 16, maxX = 0, maxY = 0;
		
		for (Letter letter : playerMove.getWord()) {
			Point pos = letter.getLetterPosition();
			if (pos.x > maxX) {
				maxX = pos.x;
			}
			if (pos.x < minX) {
				minX = pos.x;
			}
			if (pos.y > maxY) {
				maxY = pos.y;
			}
			if (pos.y < minY) {
				minY = pos.y;
			}
		}
		
		int newStartX = minX;
		int newStartY = minY;
		int newEndX = maxX;
		int newEndY = maxY;
		
		if (orientation == Direction.Horizontal) {
			while(newStartX > MIN_INDEX && getCharacterAt(newStartX - 1, newStartY) != null) {
				newStartX--;
			}
			while(newEndX < MAX_INDEX && getCharacterAt(newEndX + 1, newEndY) != null) {
				newEndX++;
			}
		} else {
			while(newStartY > MIN_INDEX && getCharacterAt(newStartX, newStartY - 1) != null) {
				newStartY--;
			}
			while(newEndY < MAX_INDEX && getCharacterAt(newEndX, newEndY + 1) != null) {
				newEndY++;
			}
		}
		
		if (newStartX == minX && newStartY == minY && newEndX == maxX && newEndY == maxY) {
			return playerMove;
		}
		
		boolean isExistingChars = false;
		StringBuilder builder = new StringBuilder(encodeCoordinates(newStartX, newStartY, orientation));
		builder.append(" ");
		for (int x = newStartX; x < minX; x++) {
			if (!isExistingChars) {
				builder.append("(");
				isExistingChars = true;
			}
			builder.append(getCharacterAt(x, newStartY));
		}
		for (int y = newStartY; y < minY; y++) {
			if (!isExistingChars) {
				builder.append("(");
				isExistingChars = true;
			}
			builder.append(getCharacterAt(newStartX, y));
		}
		for (Letter letter : playerMove.getWord()) {
			if (isExistingChars && letter.getIsEligibleForBonuses()) {
				builder.append(")");
				isExistingChars = false;
			} else if (!isExistingChars && !letter.getIsEligibleForBonuses()) {
				builder.append("(");
				isExistingChars = true;
			}
			builder.append(letter.getLetter());
		}
		for (int x = maxX + 1; x <= newEndX; x++) {
			if (!isExistingChars) {
				builder.append("(");
				isExistingChars = true;
			}
			builder.append(getCharacterAt(x, newEndY));
		}
		for (int y = maxY + 1; y <= newEndY; y++) {
			if (!isExistingChars) {
				builder.append("(");
				isExistingChars = true;
			}
			builder.append(getCharacterAt(newEndX, y));
		}
		if (isExistingChars) {
			builder.append(")");
		}
		
		Move returnMove = new Move(builder.toString(), playerMove.getMoveOrder());
		returnMove.setPlayerLogin(playerMove.getPlayerLogin());
		returnMove.setTimeStamp(playerMove.getTimeStamp());
		return returnMove;
	}
	
	/**
	 * Gets the String encoding for given X and Y coordinates and direction
	 * @param x The X coord to encode
	 * @param y The Y coord to encode
	 * @param direction The direction of the encoding
	 * @return The proper string encoding for the given coordinates
	 */
	private String encodeCoordinates(int x, int y, Direction direction)
	{
		StringBuilder builder = new StringBuilder();
		char xEncoding = (char) ('A' + (x - 1));
		
		if (direction == Direction.Horizontal) {
			builder.append(y);
			builder.append(xEncoding);
		} else {
			builder.append(xEncoding);
			builder.append(y);
		}
		
		return builder.toString();
	}
    
    /**
     * Commits the given Move to this board, if valid.
     * @param playerMove The move to commit to this board.
     */
    public void commitMove(Move playerMove)
    {
        // Save committed letters in case we need to revert them.
        List<Letter> committedLetters = new ArrayList<>();
        
        for (Letter letter : playerMove.getWord()) {
            Point letterPos = letter.getLetterPosition();
            Character existingChar = getCharacterAt(letterPos.x, letterPos.y);
            
            if (!letter.getIsEligibleForBonuses()) {
                // Make sure the letters on the board match what's in the move.
                if (existingChar == null) {
                    revertLetters(committedLetters);
                    throw new IllegalArgumentException("Existing char not found where expected");
                } else if (!existingChar.equals(letter.getLetter())){
                    revertLetters(committedLetters);
                    throw new IllegalArgumentException("Existing char not matching what expected");
                }
            } else {
                setCharacterAt(letterPos.x, letterPos.y, letter.getLetter());
                committedLetters.add(letter);
            }
        }
    }
    
    /**
     * Utility for removing committed letters from the board
     * @param letters The letters to remove from this board
     */
    private void revertLetters(Iterable<Letter> letters)
    {
        Point letterPos;
        for(Letter letter : letters) 
        {
            letterPos = letter.getLetterPosition();
            // Null indicates no letter is found there.
            if (letter.getIsEligibleForBonuses()) {
                setCharacterAt(letterPos.x, letterPos.y, null);
            }
        }
    }
    
    /**
     * For a given letter and a given orientation of the original word 
     * containing that letter, get the word containing the same letter and of 
     * the opposite orientation, if any.
     * @param letter The letter to examine
     * @param orientation The orientation of the word containing letter
     * @return The word containing the same letter and opposite orientation. 
     * Else, returns null.
     */
    private Word getSideWord(Letter letter, Direction orientation)
    {
        if (!hasSideWord(letter, orientation)) {
            // A null word indicates no side word was found.
            return null;
        }
        
        Word sideWord = new Word();
        
        int curX, curY, baseX, baseY;
        int plusX = 0;
        int plusY = 0;
        
        curX = baseX = letter.getLetterPosition().x;
        curY = baseY = letter.getLetterPosition().y;
       
        // If the original word is horizontal, then we search vertically 
        // (plusY = 1). Otherwise, we search horizontally (plusX = 1).
        if (orientation == Direction.Horizontal) {
            plusY = 1;
        } else {
            plusX = 1;
        }
        
        // Search backwards from the current letter (in the opposite orientation)
        // until we find the first row of the board or a blank.
        while(curX > MIN_INDEX && curY > MIN_INDEX && 
                getCharacterAt(curX - plusX, curY - plusY) != null) {
			curX -= plusX;
			curY -= plusY;
		}
        
        // Walk forwards from the first letter (in the opposite orientation) 
        // until we get back to the original starting letter.
        while(curX != baseX || curY != baseY) 
        {
            // Add found letters to our word
            Character curChar = getCharacterAt(curX, curY);
            sideWord.addLetter(new Letter(curX, curY, curChar, false));
            curX += plusX;
            curY += plusY;
        }
        
        // Add the original letter to the result.
        sideWord.addLetter(letter);
        curX += plusX;
        curY += plusY;
        
        // Continue to walk forard until the end of the board or a blank square.
        while (curX <= MAX_INDEX && curY <= MAX_INDEX) {
            Character curChar = getCharacterAt(curX, curY);
            if (curChar == null) {
                break;
            }
            
            // Add any found letters to our returned word.
            sideWord.addLetter(new Letter(curX, curY, curChar, false));
            curX += plusX;
            curY += plusY;
        }
        
        return sideWord;
    }
    
    /**
     * For a given letter and an orientation, attempts to find a word on the 
     * board using the opposite orientation and the same letter.
     * @param letter The letter to examine
     * @param orientation The orientation of the original word containing the 
     * specified letter.
     * @return True iff a side word is found. Else, false.
     */
    private boolean hasSideWord(Letter letter, Direction orientation)
    {
        if (!letter.getIsEligibleForBonuses()) {
            return false;
        }
        
        int plusX = 0;
        int plusY = 0;
        
        if (orientation == Direction.Horizontal) {
            plusY = 1;
        } else {
            plusX = 1;
        }
        
        int xBase = letter.getLetterPosition().x;
        int yBase = letter.getLetterPosition().y;
        
        if (xBase + plusX <= MAX_INDEX && yBase + plusY <= MAX_INDEX &&
                getCharacterAt(xBase + plusX, yBase + plusY) != null) {
            return true;
        }
        
        if (xBase - plusX >= MIN_INDEX && yBase - plusY >= MIN_INDEX &&
                getCharacterAt(xBase - plusX, yBase - plusY) != null) {
            return true;
        }
        
        return false;
    }
}
