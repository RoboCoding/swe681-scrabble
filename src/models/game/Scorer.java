package models.game;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Scorer
 * Class capable of scoring words in traditional Scrabble rules.
 * @author Owner
 */
public class Scorer
{
    // The constant additive bonus for words that use the maximum number of 
    // letters.
    private static final int LARGE_WORD_BONUS = 50;
    
    // The points at which words score double points.
    private Set<Point> _doubleWordScore;
    // The points at which words score triple points.
    private Set<Point> _tripleWordScore;
    // The points at which letters score double points.
    private Set<Point> _doubleLetterScore;
    // The points at which letters score triple points.
    private Set<Point> _tripleLetterScore;
    
    /**
     * Initializes a new instance of the Scorer class.
     */
    public Scorer()
    {
		_doubleWordScore = new HashSet<>();
		_tripleWordScore = new HashSet<>();
		_doubleLetterScore = new HashSet<>();
		_tripleLetterScore = new HashSet<>();
        initializeModifierSets();
    }
    
    /**
     * Gets a score (without considering positioning) for the specified 
     * collection of characters.
     * @param characters The characters to score without positional information.
     * @return The numerical score for the specified characters.
     */
    public int getScore(Iterable<Character> characters)
    {
        int returnValue = 0;
        
        for (Character character : characters) {
            returnValue += getCharacterScore(character);
        }
        
        return returnValue;
    }
    
    /**
     * Gets a score (considering positioning) for the specified word.
     * @param word The word to score with positional information.
     * @return The numerical score for the specified characters.
     */
    public int getScore(Word word)
    {
        int wordBonus = 1;
        int baseWordScore = 0;
        int playerLetterCount = 0 ;
        int returnValue;
        
        for (Letter letter : word) {
            wordBonus *= getWordBonus(letter);
            baseWordScore += getLetterScore(letter);
            if (letter.getIsEligibleForBonuses()) {
                playerLetterCount++;
            }
        }
        
        returnValue = wordBonus * baseWordScore;
        
        if (playerLetterCount == 7) {
            returnValue += LARGE_WORD_BONUS;
        }
        
        return returnValue;
    }
    
    /**
     * Returns the word-score multiplier accrued by the given letter.
     * @param letter The letter to examine for word bonuses.
     * @return Number indicating the word-score multiplier (1, 2, or 3)
     */
    private int getWordBonus(Letter letter)
    {
        int bonus = 1;
        
        if (letter.getIsEligibleForBonuses()) {
            if (_doubleWordScore.contains(letter.getLetterPosition())) {
                bonus = 2;
            } else if (_tripleWordScore.contains(letter.getLetterPosition())) {
                bonus = 3;
            }
        }
        
        return bonus;
    }
    
    /**
     * Returns the letter-score multiplier accrued by the given letter.
     * @param letter The letter to examine for letter bonuses.
     * @return Number indicating the letter-score multiplier (1, 2, or 3)
     */
    private int getLetterBonus(Letter letter)
    {
        int bonus = 1;
        
        if (letter.getIsEligibleForBonuses()) {
            if (_doubleLetterScore.contains(letter.getLetterPosition())) {
                bonus *= 2;
            } else if (_tripleLetterScore.contains(letter.getLetterPosition())) {
                bonus *= 3;
            }
        }
        
        return bonus;
    }
    
    /**
     * Returns the score for the specified letter, adjusted by position.
     * @param letter The letter to score
     * @return The score for the given letter (taking letter-score adjustments 
     * into account)
     */
    private int getLetterScore(Letter letter)
    {
        int bonus = getLetterBonus(letter);
        int baseScore = getCharacterScore(letter.getLetter());
        return bonus * baseScore;
    }
    
    /**
     * Returns the Scrabble scoring value for a given character.
     * @param character The character to score.
     * @return The score for the character.
     */
    private int getCharacterScore(Character character)
    {
        switch (character) {
            case 'A':
            case 'E':
            case 'I':
            case 'O':
            case 'N':
            case 'R':
            case 'T':
            case 'L':
            case 'S':
            case 'U':
                return 1;
            case 'D':
            case 'G':
                return 2;
            case 'B':
            case 'C':
            case 'M':
            case 'P':
                return 3;
            case 'F':
            case 'H':
            case 'V':
            case 'W':
            case 'Y':
                return 4;
            case 'K':
                return 5;
            case 'J':
            case 'X':
                return 8;
            case 'Q':
            case 'Z':
                return 10;
            default:
                throw new IllegalArgumentException("Unknown letter found!");
        }
    }
    
    /**
     * Populates the internal structures of this to represent the bonus 
     * coordinates of all bonuses on a standard Scrabble board.
     */
    private void initializeModifierSets()
    {
        _doubleWordScore.add(new Point(2,2));
        _doubleWordScore.add(new Point(3,3));
        _doubleWordScore.add(new Point(4,4));
        _doubleWordScore.add(new Point(5,5));
        _doubleWordScore.add(new Point(14,2));
        _doubleWordScore.add(new Point(13,3));
        _doubleWordScore.add(new Point(12,4));
        _doubleWordScore.add(new Point(11,5));
        _doubleWordScore.add(new Point(2,14));
        _doubleWordScore.add(new Point(3,13));
        _doubleWordScore.add(new Point(4,12));
        _doubleWordScore.add(new Point(5,11));
        _doubleWordScore.add(new Point(14,14));
        _doubleWordScore.add(new Point(13,13));
        _doubleWordScore.add(new Point(12,12));
        _doubleWordScore.add(new Point(11,11));
        _doubleWordScore.add(new Point(8,8));
        
        _tripleWordScore.add(new Point(1,1));
        _tripleWordScore.add(new Point(8,1));
        _tripleWordScore.add(new Point(15,1));
        _tripleWordScore.add(new Point(1,8));
        _tripleWordScore.add(new Point(15,8));
        _tripleWordScore.add(new Point(1,15));
        _tripleWordScore.add(new Point(8,15));
        _tripleWordScore.add(new Point(15,15));
        
        _doubleLetterScore.add(new Point(4,1));
        _doubleLetterScore.add(new Point(12,1));
        _doubleLetterScore.add(new Point(7,3));
        _doubleLetterScore.add(new Point(9,3));
        _doubleLetterScore.add(new Point(1,4));
        _doubleLetterScore.add(new Point(8,4));
        _doubleLetterScore.add(new Point(15,4));
        _doubleLetterScore.add(new Point(3,7));
        _doubleLetterScore.add(new Point(7,7));
        _doubleLetterScore.add(new Point(9,7));
        _doubleLetterScore.add(new Point(13,7));
        _doubleLetterScore.add(new Point(4,8));
        _doubleLetterScore.add(new Point(12,8));
        _doubleLetterScore.add(new Point(3,9));
        _doubleLetterScore.add(new Point(7,9));
        _doubleLetterScore.add(new Point(9,9));
        _doubleLetterScore.add(new Point(13,9));
        _doubleLetterScore.add(new Point(1,12));
        _doubleLetterScore.add(new Point(8,12));
        _doubleLetterScore.add(new Point(15,12));
        _doubleLetterScore.add(new Point(7,13));
        _doubleLetterScore.add(new Point(9,13));
        _doubleLetterScore.add(new Point(4,15));
        _doubleLetterScore.add(new Point(12,15));
        
        _tripleLetterScore.add(new Point(1,2));
        _tripleLetterScore.add(new Point(1,2));
        _tripleLetterScore.add(new Point(2,6));
        _tripleLetterScore.add(new Point(6,6));
        _tripleLetterScore.add(new Point(10,6));
        _tripleLetterScore.add(new Point(14,6));
        _tripleLetterScore.add(new Point(2,10));
        _tripleLetterScore.add(new Point(6,10));
        _tripleLetterScore.add(new Point(10,10));
        _tripleLetterScore.add(new Point(14,10));
        _tripleLetterScore.add(new Point(6,14));
        _tripleLetterScore.add(new Point(10,14));
    }
}
