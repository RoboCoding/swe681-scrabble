package models.game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.TreeSet;

/**
 * Dictionary
 * A class for looking up valid Scrabble words. 
 */
public final class Dictionary {
    
    // Collection of valid words.
    private static final Set<String> _validWords = new TreeSet<>();
    // Path to the dictionary file.
    private static final String DICTIONARY_PATH = "/models/game/dictionary.txt";
    
    /**
     * Static type constructor for Dictionary
     */
    static
    {
        try (InputStream fis =
				Class.forName("models.game.Dictionary").getResourceAsStream(DICTIONARY_PATH))
        {
            try (InputStreamReader isr = new InputStreamReader(fis))
            {
                try (BufferedReader reader = new BufferedReader(isr))
                {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        _validWords.add(line.trim().toUpperCase());
                    }
                } catch (IOException e) {
                    // What to do here?
                    //Insert logger call
                }
            } catch (IOException e) {
                // What to do here?
                //Insert logger call
            }
        } catch (IOException | ClassNotFoundException e) {
            //Insert logger call
        }
    }

    private Dictionary()
    {
        //empty
    }
    
    /**
     * Checks whether the given word is in this dictionary.
     * @param wordToCheck The word to lookup in this dictionary
     * @return True iff wordToCheck is in this dictionary. Else, false.
     */
    public static boolean contains(String wordToCheck)
    {
        return _validWords.contains(wordToCheck.toUpperCase());
    }
}
