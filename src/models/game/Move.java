package models.game;

import models.dao.MoveState;
import java.awt.*;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * Move 
 * Abstraction for a single player's move in Scrabble.
 */
public class Move
{
	// The globally unique ID of this move.
	private int _uniqueMoveID;
	// The ID of this move.
	private int _moveID;
	// The player that made this move.
	private String _playerLogin;
	// The score for this move.
	private int _moveScore;
	// The point for the first letter in the word of this move.
	private Point _startingCoord;
	// Whether the word of this move flows horizontal or vertical.
	private Direction _orientation;
	// The word represented by this move
	private final Word _word;
	// The exact code for this word
	private final String _wordCoding;
	// The timestamp of this move
	private Date _timeStamp;
	// Whether this move is a skip move
	private boolean _isSkipMove = false;

	// A regular expression to determine the syntactic validity of a move.
	private static final Pattern MOVE_PATTERN = Pattern.compile(
			"^(PASS|(([A-O][0-1]?[0-9]{1}|[0-1]?[0-9]{1}[A-O]) ([a-zA-Z]*\\(?[a-zA-Z]*\\)?[a-zA-Z]*)*))$");

	/**
	 * Static operation that determines if a move coding is syntactically valid.
	 *
	 * @param possiblyMove The move text to examine.
	 * @return True iff the move text is valid. Else, false.
	 */
	public static boolean isValid(String possiblyMove)
	{
		return MOVE_PATTERN.matcher(possiblyMove).matches();
	}

	/**
	 * Initializes a new instance of the Move class.
	 *
	 * @param moveCoding The move coding represented by this class.
	 * @throws IllegalArgumentException Thrown if the move coding is not valid.
	 */
	public Move(String moveCoding, int order) throws IllegalArgumentException
	{
		_word = new Word();
		_wordCoding = moveCoding;
		_moveID = order;
		decodeMove(moveCoding);
		_timeStamp = new Date();
	}

	/**
     * Initializes a new instance of the Move class.
     * @param move The move represented by this class.
     * @throws IllegalArgumentException Thrown if the move coding is not valid.
     */
	public Move(MoveState move)
	{
		this(move.getMoveText(), move.getMoveId());
		_uniqueMoveID = move.getUniqueId();
		_timeStamp = move.getTimestamp();
		_playerLogin = move.getPlayer();
		_moveScore = move.getScore();
	}

	/**
	 * Converts this move to the corresponding MoveState
	 * @return The MoveState corresponding to this.
	 */
	public MoveState toMoveState()
	{
		MoveState state = new MoveState();
		
		state.setMoveId(_moveID);
		state.setUniqueId(_uniqueMoveID);
		state.setMoveText(_wordCoding);
		state.setTimestamp(_timeStamp);
		state.setScore(_moveScore);
		
		return state;
	}

	/**
	 * Gets the order number of this move
	 * @return The order of this move
	 */
	public int getMoveOrder()
	{
		return _moveID;
	}

	/**
	 * Gets the starting coordinate for the word in this move.
	 *
	 * @return A Point representing the location of the first letter in this
	 * word.
	 */
	public Point getStartingCoordinate()
	{
		return _startingCoord;
	}

	/**
	 * Gets the direction in which this word flows.
	 *
	 * @return A Direction enum value representing the direction this word
	 * flows.
	 */
	public Direction getOrientation()
	{
		return _orientation;
	}

	/**
	 * Gets the word represented by this move.
	 *
	 * @return The Word representing this move.
	 */
	public Word getWord()
	{
		return _word;
	}

	/**
	 * Gets the move coding that construct this move.
	 *
	 * @return The string that was used to construct this move.
	 */
	public String getMoveCoding()
	{
		return _wordCoding;
	}
	
	/**
	 * Gets the timestamp for this move.
	 * @return Date object containing the timestamp for this move.
	 */
	public Date getTimeStamp()
	{
		return _timeStamp;
	}
	
	/**
	 * Sets the timestamp for this move.
	 * @param timeStamp The timestamp for this move.
	 */
	public void setTimeStamp(Date timeStamp)
	{
		_timeStamp = timeStamp;
	}
	
	/**
	 * Gets the player that made this move
	 * @return String representing the login of the player that made this move.
	 */
	public String getPlayerLogin()
	{
		return _playerLogin;
	}
	
	/**
	 * Sets the player that made this move
	 * @param login String representing the login of the player that made this move.
	 */
	public void setPlayerLogin(String login)
	{
		_playerLogin = login;
	}
	
	/**
	 * Gets the score for this move.
	 * @return The score for this move.
	 */
	public int getScore()
	{
		return _moveScore;
	}
	
	/**
	 * Sets the score for this move.
	 * @param score The score for this move.
	 */
	public void setScore(int score)
	{
		_moveScore = score;
	}
	
	/**
	 * Gets the ID of this move
	 * @return The ID of this move
	 */
	public int getMoveId()
	{
		return _moveID;
	}
	
	/**
	 * Gets whether this move represents a Pass move.
	 * @return True if this move represents a pass move.
	 */
	public boolean getIsPassMove()
	{
		return _isSkipMove;
	}
	
	/**
	 * Configures this move to be a PASS move.
	 */
	private void setPassMove()
	{
		_moveScore = 0;
		_isSkipMove = true;
	}

	/**
	 * Parses the specified move and initializes the state of this.
	 *
	 * @param moveCoding The move coding to parse.
	 * @throws IllegalArgumentException Thrown if the move is not valid.
	 */
	private void decodeMove(String moveCoding)
	{
		// First use the regular expression to filter out obviously bad data.
		if (!isValid(moveCoding)) {
			throw new IllegalArgumentException("Invalid move sequence");
		}
		
		if (moveCoding.equalsIgnoreCase("PASS")) {
			setPassMove();
			return;
		}

		int xCoordStart, yCoordStart, yCoordEnd, xCoord, yCoord, wordStart;

		Character xCoordCode;
		String yCoordCode;

		// Moves that have a coordinate like 1B are horizontal. B1 is vertical.
		if (Character.isDigit(moveCoding.charAt(0))) {
			_orientation = Direction.Horizontal;

			yCoordStart = 0;

			if (Character.isDigit(moveCoding.charAt(1))) {
				yCoordEnd = 1;
			} else {
				yCoordEnd = 0;
			}

			xCoordStart = yCoordEnd + 1;
			wordStart = xCoordStart + 2;
		} else {
			_orientation = Direction.Vertical;

			xCoordStart = 0;
			yCoordStart = 1;

			if (Character.isDigit(moveCoding.charAt(2))) {
				yCoordEnd = 2;
			} else {
				yCoordEnd = 1;
			}

			wordStart = yCoordEnd + 2;
		}

		xCoordCode = moveCoding.charAt(xCoordStart);
		yCoordCode = moveCoding.substring(yCoordStart, yCoordEnd + 1);

		xCoord = xCoordCode - 'A' + 1;
		yCoord = Integer.parseInt(yCoordCode);

		if (xCoord < 1 || xCoord > 15) {
			throw new IllegalArgumentException("Illegal X coordinate");
		}

		if (yCoord < 1 || yCoord > 15) {
			throw new IllegalArgumentException("Illegal Y coordinate");
		}

		_startingCoord = new Point(xCoord, yCoord);

		String subWord = moveCoding.substring(wordStart);
		boolean isUserCharacter = true;

		int plusX = 0;
		int plusY = 0;

		if (_orientation == Direction.Horizontal) {
			plusX = 1;
		} else {
			plusY = 1;
		}

		int notControlChars = 0;
		for (int i = 0; i < subWord.length(); i++) {
			char nextChar = subWord.charAt(i);

			if (nextChar == '(') {
				if (!isUserCharacter) {
					throw new IllegalArgumentException("Illegal word definition");
				}
				isUserCharacter = false;
				continue;

			} else if (nextChar == ')') {
				if (isUserCharacter) {
					throw new IllegalArgumentException("Illegal word definition");
				}
				isUserCharacter = true;
				continue;
			}

			int xLetter = _startingCoord.x + plusX * notControlChars;
			int yLetter = _startingCoord.y + plusY * notControlChars;

			if (xLetter > 15 || yLetter > 15) {
				throw new IllegalArgumentException("Word too long");
			}

			_word.addLetter(new Letter(xLetter, yLetter, nextChar, isUserCharacter));
			notControlChars++;
		}
	}
}
