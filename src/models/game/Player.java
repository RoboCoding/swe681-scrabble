package models.game;

import models.dao.PlayerOrderState;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Player
 * Abstraction for a player in a Scrabble game.
 */
public class Player 
{
	// The identification for this player.
	private String _playerLogin;
    // The letters on the players rack.
    private final List<Character> _letters = new ArrayList<>();
    // The player's current score.
    private int _score;
	// The player's unique order ID
	private int _orderID;
	// The player's order of play
	private int _orderOfPlay;
    
	/**
	 * Initializes a new instance of the Player class with the given login.
	 * @param playerLogin The login for this player.
	 */
	public Player(String playerLogin)
	{
		_playerLogin = playerLogin;
	}
	
	/**
	 * Initializes a new instance of the Player class from the given state.
	 * @param playerState The state of the player to configure this.
	 */
	public Player(PlayerOrderState playerState)
	{
		this(playerState.getPlayer());
		_orderID = playerState.getOrderId();
		_orderOfPlay = playerState.getOrder();
	}
	
	/**
	 * Converts this Player to an appropriate PlayerOrderState object.
	 * @return The PlayerOrderState object corresponding to this.
	 */
	public PlayerOrderState toPlayerState()
	{
		PlayerOrderState state = new PlayerOrderState();
		state.setPlayer(_playerLogin);
		state.setOrderId(_orderID);
		state.setOrder(_orderOfPlay);
		return state;
	}
	
	/**
	 * Gets the order of play for this person
	 * @return The order of play for this person
	 */
	public int getOrder()
	{
		return _orderOfPlay;
	}
	
	/**
	 * Gets the login for this player
	 * @return The String representing the player's login
	 */
	public String getPlayerLogin()
	{
		return _playerLogin;
	}
	
	/**
	 * Sets the login for this player
	 * @param login The String representing the player's login
	 */
	public void setPlayerLogin(String login)
	{
		_playerLogin = login;
	}
	
    /**
     * Returns the number of letters in the player's rack.
     * @return The number of letters in the player's rack.
     */
    public int getLetterCount()
    {
        return _letters.size();
    }
    
    /**
     * Return whether this player has any characters remaining.
     * @return True if this player has 1 or more letters. Else, false.
     */
    public boolean hasLetters()
    {
        return !_letters.isEmpty();
    }
    
    /**
     * Returns the letters in this player's rack.
     * @return An iterable collection of letters in this player's rack.
     */
    public Iterable<Character> getLetters()
    {
        return _letters;
    }
    
    /**
     * Returns the player's current score
     * @return The current score of the player.
     */
    public int getScore()
    {
        return _score;
    }
    
    /**
     * Adds the specified amount to this player's score.
     * @param score The non-negative amount to add to this player's score.
     */
    public void addScore(int score)
    {
        _score += score;
    }
    
    /**
     * Removes the specified amount to this player's score.
     * @param score The non-negative amount to remove from this player's score.
     */
    public void removeScore(int score)
    {
        _score -= score;
    }
    
    /**
     * Determines whether this player can play the specified word.
     * @param word The word to validate against this player's rack of letters.
     * @return True iff the word is valid. Else, false.
     */
    public boolean isWordValid(Word word)
    {
        List<Character> lettersCopy = new ArrayList<>(_letters);
        return tryRemoveCharacters(lettersCopy, getPlayerLetters(word));
    }
    
    /**
     * Adds the specified letters to the player's rack.
     * @param newLetters The letters to add to the player's rack.
     */
    public void addLetters(Iterable<Character> newLetters) {
        for (Character letter : newLetters) {
            _letters.add(letter);
        }
    }
    
    /**
     * Removes the letters from the specified word from the player's rack.
     * @param word The word of letters to remove from this player's rack.
     */
    public void subtractLetters(Word word)
    {
        if (!isWordValid(word)) {
            throw new IllegalArgumentException("Don't hold all letters that tried to remove");
        }
        
        tryRemoveCharacters(_letters, getPlayerLetters(word));
    }
    
    /**
     * Removes the specified characters from this student's rack and determines 
     * whether this removal is valid.
     * @param collection The collection from which to remove letters.
     * @param charsToRemove The letters to remove from the specified collection.
     * @return True if all letters were removed. Else, false.
     */
    private boolean tryRemoveCharacters(
            Collection<Character> collection, Iterable<Character> charsToRemove)
    {
        for (Character letter : charsToRemove) {
            if (!collection.remove(letter)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Returns the characters from the specified word that were just-played.
     * @param word The word to examine
     * @return An iterable collection of characters representing just-played 
     * characters.
     */
    private Iterable<Character> getPlayerLetters(Word word)
    {
        List<Character> returnValue = new ArrayList<>();
        for (Letter letter : word) {
            if (letter.getIsEligibleForBonuses()) {
                returnValue.add(letter.getLetter());
            }
        }
        return returnValue;
    }
}
