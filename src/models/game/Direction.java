package models.game;

/**
 * Direction
 * Enumeration of orientations for a word on a Scrabble board.
 */
public enum Direction 
{
    Vertical,
    Horizontal
}
