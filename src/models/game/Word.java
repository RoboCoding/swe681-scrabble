package models.game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Word
 * Abstraction of a Word, which is a series of Letters on a Scrabble board.
 * @author Owner
 */
public class Word implements Iterable<Letter> 
{
    // The letters contained in this Word.
    private final List<Letter> _letters;
    
    /**
     * Initializes a new instance of the Word class.
     */
    public Word()
    {
        _letters = new ArrayList<Letter>();
    }
    
    /**
     * Returns the string representation of this word.
     * @return A string composed of the Character associated with each letter.
     */
    public String getWord()
    {
        StringBuffer word = new StringBuffer();
        for (Letter letter : this) {
            word.append(letter.getLetter());
        }
        return word.toString();
    }
    
    /**
     * Adds a letter to this word.
     * @param letter The letter to add to this word.
     */
    public void addLetter(Letter letter)
    {
        _letters.add(letter);
    }
    
    /**
     * Returns the iterator of letters for this word.
     * @return The iterator for the letters in this word.
     */
    @Override
    public Iterator<Letter> iterator()
    {
        return _letters.iterator();
    }
}
