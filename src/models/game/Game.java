package models.game;

import models.dao.GameState;
import models.dao.MoveState;
import models.dao.PlayerOrderState;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Game An abstraction for the data about a Scrabble game.
 */
public class Game
{
	// The ID of the game
	private int _gameID;

	// The maximum number of letters a player can have.
	private static final int MAX_LETTER_COUNT = 7;
	
	// The time in milliseconds to wait since last move 
	// before a player loses a game.
	private static final long MAX_TIME_WAIT = 300000;

	// The Scrabble game board.
	private final Board _gameBoard;

	// The collection of moves for this game.
	private final List<Move> _moves;

	// The collection of players in this game.
	private final List<Player> _players;

	// The pouch of letters for this game.
	private final LetterPouch _letterPouch;

	// The class for scoring moves
	private final Scorer _scoringSystem;

	// The number of players in the game.
	private int _numPlayers = 0;

	// The number of the player whose turn it is
	private int _playerTurnNumber = 1;

	// Whether this game has started.
	private boolean _gameIsStarted = false;
	
	// Whether this game is currently finished.
	private boolean _gameIsOver = false;

	// The player that won the game.
	private Player _winningPlayer;
	
	// When the game was started
	private Date _timeStamp;

	/**
	 * Initializes a new instance of the Game class.
	 */
	public Game()
	{
		_gameBoard = new Board();
		_players = new ArrayList<>();
		_moves = new ArrayList<>();
		_letterPouch = new LetterPouch();
		_scoringSystem = new Scorer();
		_numPlayers = 0;
		_playerTurnNumber = 1;
	}

	/**
	 * Initializes a new instance of the Game class from the given state.
	 * @param gameState 
	 */
	public Game(GameState gameState)
	{		
		_gameBoard = new Board();
		_players = new ArrayList<>();
		_moves = new ArrayList<>();
		_letterPouch = new LetterPouch(gameState.getLetterPouch());
		_scoringSystem = new Scorer();
		_gameID = gameState.getGameId();
		_timeStamp = gameState.getStartingTimestamp();
		
		for (PlayerOrderState player : gameState.getPlayers()) {
			Player convertedPlayer = new Player(player);
			int order = player.getOrder();
			for (int i = 0; i <= _players.size(); i++) {				
				if (i != _players.size() && order > _players.get(i).getOrder()) {
					continue;
				}
				_players.add(i, convertedPlayer);
				break;
			}
			if (convertedPlayer.getPlayerLogin().equalsIgnoreCase(gameState.getWinningPlayer())) {
				_winningPlayer = convertedPlayer;
			}
		}
		
		_numPlayers = _players.size();
		_playerTurnNumber = 1;
		
		_gameIsStarted = gameState.getIsGameStarted();
		
		if (_gameIsStarted) {
			for (Player player : _players) {
				player.addLetters(_letterPouch.getLetters(MAX_LETTER_COUNT));
			}
		}

		for (MoveState move : gameState.getMoves()) {
			Move convertedMove = new Move(move);
			for (int i = 0; i <= _moves.size(); i++) {				
				if (i != _moves.size() && convertedMove.getMoveId() > _moves.get(i).getMoveId()) {
					continue;
				}
				_moves.add(i, convertedMove);
				break;
			}
		}
		
		for (int i = 0; i < _moves.size(); i++) {
			processMove(_moves.get(i), new ArrayList<String>());
		}		
		_gameIsOver = gameState.getIsGameOver();
	}
	
	/**
	 * Converts this Game to an appropriate GameState object.
	 * @return The GameState configured to be the same as this.
	 */
	public GameState toGameState()
	{
		GameState state = new GameState();
		
		state.setGameId(_gameID);
		state.setIsGameStarted(_gameIsStarted);
		state.setIsGameOver(_gameIsOver);
		state.setLetterPouch(_letterPouch.getAllContents());
		state.setStartingTimestamp(_timeStamp);
		
		List<PlayerOrderState> orderStates = new ArrayList<>();
		for (int i = 0; i < _players.size(); i++) {
			Player player = _players.get(i);
			PlayerOrderState pos = player.toPlayerState();
			pos.setOrder(i);
			if (_gameIsOver && _winningPlayer != null && _winningPlayer.equals(player)) {
				state.setWinningPlayer(pos.getPlayer());
			}
			orderStates.add(pos);
		}
		state.setPlayers(orderStates);
		
		List<MoveState> moveStates = new ArrayList<>();
		for (int i = 0; i < _moves.size(); i++) {
			Move move = _moves.get(i);
			MoveState ms = move.toMoveState();
			int playerMoveIndex = (1 + i) % _numPlayers;
			ms.setPlayer(orderStates.get(playerMoveIndex).getPlayer());
			moveStates.add(ms);
		}
		state.setMoves(moveStates);
		return state;
	}
	
	/**
	 * Attempts to start this Scrabble game.
	 * @param errors Errors that occur while trying to start the game.
	 * @return True if the game starts. Else, false.
	 */
	public boolean startGame(List<String> errors)
	{
		if (_gameIsStarted) {
			errors.add("Can't start a game already in progress!");
			return false;
		}
		
		if(_gameIsOver) {
			errors.add("Can't start a game that has already ended!");
			return false;
		}
		
		if (_numPlayers < 2) {
			errors.add("Not enough players to start the game");
			return false;
		}
		
		List<Player> sortedPlayerList = new ArrayList<>();
		List<Character> sortedLetters = new ArrayList<>();
		
		for (Player player : _players) {
			Character startingLetter = _letterPouch.getRandomLetter();
			boolean addedPlayer = false;
			for (int i = 0; i < sortedLetters.size(); i++) {
				if (Character.compare(startingLetter, sortedLetters.get(i)) <= 0) {
					sortedPlayerList.add(i, player);
					sortedLetters.add(startingLetter);
					addedPlayer = true;
					break;
				}
			}
			if (!addedPlayer) {
				sortedPlayerList.add(player);
				sortedLetters.add(startingLetter);
			}
		}
		
		_players.clear();
		_players.addAll(sortedPlayerList);
		
		for (Player player : _players) {
			player.addLetters(_letterPouch.getLetters(MAX_LETTER_COUNT));
		}
		
		_gameIsStarted = true;
		_timeStamp = new Date();
		return true;
	}
	
	/**
	 * Attempts to add the specified player to this Game.
	 * @param playerLogin The login for the player in this game.
	 * @param errors Errors that occur while adding this player.
	 * @return True if the player was added. False on any error.
	 */
	public boolean addPlayer(String playerLogin, List<String> errors)
	{
		if (_gameIsStarted) {
			errors.add("Can't add players once the game has started!");
			return false;
		}
		
		if (playerLogin == null || playerLogin.isEmpty()) {
			errors.add("No player name was specified!");
			return false;
		}
		
		if (_numPlayers >= 4) {
			errors.add("Too many players in the game already!");
			return false;
		}
		
		for (Player player : _players) {
			if (player.getPlayerLogin().equalsIgnoreCase(playerLogin)) {
				errors.add("Player already in the game!");
				return false;
			}
		}
		
		Player newPlayer = new Player(playerLogin);
		_numPlayers++;
		_players.add(newPlayer);
		return true;
	}
	
	/**
	 * Sets the Game ID
	 * @param gameId The ID of the game
	 */
	public void setGameId(int gameId)
	{
		_gameID = gameId;
	}
	
	/**
	 * Gets the Game ID
	 * @return The ID of the game
	 */
	public int getGameId()
	{
		return _gameID;
	}

	/**
	 * Gets whether this game is started.
	 *
	 * @return True if the game is started. Else, false.
	 */
	public boolean getIsGameStarted()
	{
		return _gameIsStarted;
	}
	
	/**
	 * Gets whether this game is over.
	 *
	 * @return True if the game is over. Else, false.
	 */
	public boolean getIsGameOver()
	{
		return _gameIsOver;
	}

	/**
	 * Gets the player that won the game (or null if game is not over)
	 *
	 * @return The player that won the game. Else, null if the game is not over.
	 */
	public Player getWinningPlayer()
	{
		if (!getIsGameOver()) {
			throw new IllegalStateException("Game is not over yet");
		}
		return _winningPlayer;
	}
	
	/**
	 * Gets when this game was first started
	 * @return The Date representing when this game was started
	 */
	public Date getGameStartTimestamp()
	{
		return _timeStamp;
	}
	
	/**
	 * Gets the timestamp of the last move (how long the game has been idle)
	 * @return Date representing the timestamp of the most recent move (or the 
	 * time the game was created)
	 */
	public Date getLastMoveTimestamp()
	{
		Date lastEventTime;
		if (_moves.isEmpty()) {
			lastEventTime = _timeStamp;
		} else {
			lastEventTime = _moves.get(_moves.size() - 1).getTimeStamp();
		}
		return lastEventTime;
	}
	
	/**
	 * Determines whether there's been a timeout since the last move.
	 * @return True if a timeout has occurred. Else, false.
	 */
	public boolean timeOutSinceLastMove()
	{
		return timeOutSinceLastMove(new Date());
	}
	
	/**
	 * Determines whether there's been a timeout since the last move.
	 * @param timeToCheck The time to check for a timeout since last move
	 * @return True if a timeout has occurred. Else, false.
	 */
	public boolean timeOutSinceLastMove(Date timeToCheck)
	{
		Date lastEventTime = getLastMoveTimestamp();
		return timeToCheck.getTime() - lastEventTime.getTime() > MAX_TIME_WAIT;
	}
	
	/**
	 * Times out the current player and ends the game.
	 */
	public void timeoutCurrentPlayer()
	{
		// There was a move timeout, the player loses.
		List<Player> winners = new ArrayList<>();
		for (Player player : _players) {
			if (player != getCurrentPlayer()) {
				winners.add(player);
			}
		}
		endGame(winners);
	}

	/**
	 * Processes a given move and returns whether the move is valid (commits
	 * the move if it is valid).
	 *
	 * @param moveCode The string encoding of the move to process.
	 * @param errors Errors generated by analyzing the given move.
	 * @return True iff the move is valid. Else, returns false.
	 */
	public boolean processMove(String moveCode, List<String> errors)
	{
		if (getIsGameOver()) {
			// No moves are valid if the game is already over.
			errors.add("Game is already over");
			return false;
		}
		
		if (timeOutSinceLastMove()) {
			timeoutCurrentPlayer();
			return true;
		}

		Move move;
		try {
			move = new Move(moveCode, _moves.size());
		} catch (IllegalArgumentException e) {
			// The move could not be parsed.
			errors.add(e.getMessage());
			return false;
		}
		return processMove(move, errors);
	}

	/**
	 * Processes a given move and returns whether the move is valid (commits
	 * the move if it is valid).
	 *
	 * @param move The string encoding of the move to process.
	 * @param errors Errors generated by analyzing the given move.
	 * @return True iff the move is valid. Else, returns false.
	 */
	private boolean processMove(Move move, List<String> errors)
	{
		if (move.getTimeStamp()!= null && timeOutSinceLastMove(move.getTimeStamp())) {
			timeoutCurrentPlayer();
			return true;
		}
		
		if (move.getIsPassMove()) {
			if (move.getMoveOrder() >= _numPlayers - 1) {
				boolean isGameAllPassed = true;
				int curMoveIndex = move.getMoveOrder();
				for (int i = 1; i < _numPlayers; i++) {
					if (!_moves.get(curMoveIndex - i).getIsPassMove()) {
						isGameAllPassed = false;
						break;
					}
				}
				if (isGameAllPassed) {
					// Ends game with no "winners"
					if (!_moves.contains(move)) {
						_moves.add(move);
					}
					endGame(new ArrayList<Player>());
					return true;
				}
			}
			if (!_moves.contains(move)) {
				_moves.add(move);
			}
			setNextPlayerTurn();
			return true;
		}
		
		List<Word> scoringWords;
		try {
			Move expandedMove = _gameBoard.expandWord(move);
			if (!expandedMove.equals(move)) {
				move = expandedMove;
			}			
			scoringWords = _gameBoard.examineMove(move);
		} catch (IllegalArgumentException e) {
			// The move is not valid for the board state.
			errors.add(e.getMessage());
			return false;
		}
		
		if (move.getMoveOrder() == 0) { 
			boolean isError = true;
			for (Letter letter : move.getWord()) {
				Point pos = letter.getLetterPosition();
				if (pos.x == 8 && pos.y == 8) {
					isError = false;
					break;
				}
			}
			if (isError) {
				errors.add("Starting move did not use the star!");
				return false;
			}
		} else {
			boolean isError = true;
			for (Word word : scoringWords) {
				for (Letter letter : word) {
					if (!letter.getIsEligibleForBonuses()) {
						isError = false;
						break;
					}
				}
			}
			if (isError) {
				errors.add("Non-starting move did not reuse an existing letter!");
				return false;
			}
		}

		int turnScore = 0;
		for (Word word : scoringWords) {
			if (!Dictionary.contains(word.getWord())) {
				errors.add("Word not found");
				return false;
			}
			turnScore += _scoringSystem.getScore(word);
		}

		Player curPlayer = getCurrentPlayer();
		move.setScore(turnScore);

		try {
			if (!curPlayer.isWordValid(move.getWord())) {
				errors.add("Don't have all the letters we need!");
				return false;
			}
		} catch (IllegalArgumentException e) {
			// The player doesn't have the letters for the word
			errors.add(e.getMessage());
			return false;
		}

        // FROM HERE WE ASSUME ALL OPERATIONS ARE VALID
		// Commit All Changes
		curPlayer.subtractLetters(move.getWord());
		curPlayer.addScore(turnScore);
		_gameBoard.commitMove(move);
		if (!_moves.contains(move)) {
			_moves.add(move);
		}

		if (!_letterPouch.getIsBagEmpty()) {
			// Try to replace used up letters from the letter pouch.
			int neededLetterCount = MAX_LETTER_COUNT - curPlayer.getLetterCount();
			curPlayer.addLetters(_letterPouch.getLetters(neededLetterCount));
		}

		// Check for game end
		if (!curPlayer.hasLetters()) {
			endGame(curPlayer);
			return true;
		}

		// Set next player turn
		setNextPlayerTurn();
		return true;
	}

	/**
	 * Returns the player whose turn it currently is.
	 *
	 * @return The player whose turn it is.
	 */
	public Player getCurrentPlayer()
	{
		return _players.get(_playerTurnNumber - 1);
	}

	/**
	 * Returns all players playing this game.
	 * @return List containing all players from this game.
	 */
	public List<Player> getAllPlayers()
	{
		return _players;
	}
	
	/**
	 * Advances the records to indicate it is the next player's turn.
	 */
	private void setNextPlayerTurn()
	{
		_playerTurnNumber = (_playerTurnNumber % _numPlayers) + 1;
	}

	/**
	 * Execute the Scrabble end game
	 */
	private void endGame(Player winningPlayer)
	{
		endGame(Arrays.asList(winningPlayer));
	}
	
	/**
	 * Gets the contents of the game board.
	 * @return A doubly-indexed array representing the contents of the game board
	 */
	public Character[][] getBoard()
	{
		return _gameBoard.getBoardContents();
	}
	
	/**
	 * Execute the Scrabble end game
	 */
	private void endGame(Iterable<Player> winningPlayers)
	{
		List<Player> winners = new ArrayList<>();
		for (Player player : winningPlayers) {
			winners.add(player);
		}

		for (Player losingPlayer : _players) {
			if (winners.contains(losingPlayer)) {
				continue;
			}

            // Winner gets additional score equal to tiles remaining for losers.
			// Losers lose score equal to remaining tiles in their rack.
			int remainingLetterScore
					= _scoringSystem.getScore(losingPlayer.getLetters());
			losingPlayer.removeScore(remainingLetterScore);
			for (Player winner : winners) {
				winner.addScore(remainingLetterScore);
			}
		}

		int maxScore = Integer.MIN_VALUE;

		// Determine the final winning player after adjusting scores
		for (Player player : _players) {
			if (player.getScore() >= maxScore) {
				maxScore = player.getScore();
				_winningPlayer = player;
			}
		}

		_gameIsOver = true;
	}

	public Board getGameBoard()
	{
		return _gameBoard;
	}
	
	/**
	 * Gets the history of all moves from this game
	 * @return Iterable of Move representing all moves from this game
	 */
	public Iterable<Move> getAllMoves()
	{
		return _moves;
	}
}
