package models.game;

import java.awt.Point;

/**
 * Letter
 * Abstraction for a single character on a Scrabble board.
 * @author Owner
 */
public class Letter 
{
    // The coordinates at which the letter is found.
    private final Point _coord;
    // The character represented by this letter.
    private final Character _letter;
    // Whether this letter is eligible for bonuses (just vs previously played)
    private final boolean _eligibleForBonuses;
    
    /**
     * Initializes a new instance of the Letter class.
     * @param x The x coordinate at which the letter is found.
     * @param y The y coordinate at which the letter is found.
     * @param letter The character representing this letter.
     * @param justPlayed Whether this letter is just played.
     */
    public Letter(int x, int y, Character letter, boolean justPlayed)
    {
        _coord = new Point(x, y);
        _letter = letter;
        _eligibleForBonuses = justPlayed;
    }
    
    /**
     * Gets the position of this letter.
     * @return A Point representing the coordinates of this letter on the board.
     */
    public Point getLetterPosition()
    {
        return _coord;
    }
    
    /**
     * Gets the character that this letter represents.
     * @return A Character that this letter represents.
     */
    public Character getLetter()
    {
        return _letter;
    }
    
    /**
     * Gets whether this letter is eligible for bonuses (was just played).
     * @return True if this letter is eligible for bonuses. Else, false.
     */
    public boolean getIsEligibleForBonuses()
    {
        return _eligibleForBonuses;
    }
}
