package models.dao;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * PlayerOrderState
 * Persistence class representing a Player object.
 */
@Entity
@Table(name = "PlayerGame")
public class PlayerOrderState implements Serializable
{
	// Globally unique ID for this player-order
	@Id
	@GeneratedValue
	@Column(name = "OrderID")
	private int _orderID;
	
	// The name of this player
	@Column(name = "PlayerID")
	private String _playerLogin;
	
	// The order of play for this player
	@Column(name = "PlayerOrder")
	private int _order;
	
	/**
	 * Gets the globally unique ID for this order
	 * @return The globally unique ID for this order
	 */
	public int getOrderId()
	{
		return _orderID;
	}
	
	/**
	 * Sets the globally unique ID for this order
	 * @param id The globally unique ID for this order
	 */
	public void setOrderId(int id)
	{
		_orderID = id;
	}
	
	/**
	 * Gets the name of this player
	 * @return The name of this player
	 */
	public String getPlayer()
	{
		return _playerLogin;
	}
	
	/**
	 * Sets the name of this player
	 * @param player The name of this player
	 */
	public void setPlayer(String player)
	{
		_playerLogin = player;
	}
	
	/**
	 * Gets the order of play for this player.
	 * @return The order of play for this player.
	 */
	public int getOrder()
	{
		return _order;
	}
	
	/**
	 * Sets the order of play for this player
	 * @param order The order of play for this player
	 */
	public void setOrder(int order)
	{
		_order = order;
	}
}
