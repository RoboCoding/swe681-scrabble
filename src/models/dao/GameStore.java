package models.dao;

import models.game.Game;
import org.hibernate.TransactionException;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


/**
 * GameSrore
 * DAO for Game objects
 */
public final class GameStore
{
	// The EntityManager for persisting and retrieving objects.
	private static EntityManager _mgr;

	private static final int TRANSACTION_RETRIES = 3;
	
	static {
		// Static initializer ensures we only initialize this once
		_mgr = getEntityManager();
	}

	private GameStore()
	{
		// No constructor
	}


	
	/**
	 * Gets the entity manager for game objects.
	 * @return The entity manager for retrieving game objects.
	 */
	private static EntityManager getEntityManager()
	{
		EntityManagerFactory emf = 
			Persistence.createEntityManagerFactory("scrabbleGames");
		
		return emf.createEntityManager();
	}
	
	/**
	 * Gets the Game corresponding to the specified game ID.
	 * @param gameId The ID of the game to retrieve.
	 * @return The game from the DB with the specified ID. Else, null.
	 */
	public static Game importGame(int gameId)
	{
		GameState gameStore = _mgr.find(GameState.class, gameId);
		if (gameStore == null) {
			return null;
		}
		return new Game(gameStore);
	}
	
	/**
	 * Persists the specified Game to the database.
	 * @param game The game to persist in the database.
	 * @return The persisted Game instance (new Games have their IDs set)
	 */
	public static Game exportGame(Game game)
	{		
		GameState gameState = game.toGameState();

		try
		{
			_mgr.getTransaction().begin();
			gameState = _mgr.merge(gameState);
			_mgr.getTransaction().commit();
		} catch(TransactionException | RollbackException e)
		{
			return exportGame(game, TRANSACTION_RETRIES);
		}
		
		return new Game(gameState);
	}

	private static Game exportGame(Game game, int retries)
	{
		retries --;

		if(retries < 0)
		{
			return null;
		}
		else
		{
			GameState gameState = game.toGameState();

			try
			{
				_mgr.getTransaction().begin();
				gameState = _mgr.merge(gameState);
				_mgr.getTransaction().commit();
			} catch(TransactionException | RollbackException e)
			{
				return exportGame(game, retries);
			}

			return new Game(gameState);
		}
	}

	public static Iterable<String> getAllPlayersWithFinishedGame()
	{
		String getAllPlayersWithFinishedGameQuery = "SELECT DISTINCT ps._playerLogin FROM GameState AS gs JOIN gs._players AS ps WHERE" +
		                                            " gs._isGameOver = true";
		Query query = _mgr.createQuery(getAllPlayersWithFinishedGameQuery, String.class);
		List<String> playerList;
		try {
			playerList = query.getResultList();
		} catch (Exception e) {
			playerList = new ArrayList<>();
		}

		return playerList;
	}

	/**
	 * Checks to see if a user has an unstarted game.
	 * @param login the login of the user
	 * @return true iff the user has an unstarted game.
	 */
	public static boolean hasUnstartedGame(String login)
	{
		String hasUnstartedGameQuery = "SELECT gs FROM GameState gs where gs._isGameStarted = false and EXISTS" +
		                                  "(FROM gs._players pos WHERE pos._playerLogin = :login)";
		Query query = _mgr.createQuery(hasUnstartedGameQuery, GameState.class);
		query.setParameter("login", login);
		return !query.getResultList().isEmpty();
	}

	/**
	 * Finds all games that a player has joined but the game is not finished.
	 * @param login the login of the player.
	 * @return Iterable of all the games that a player has joined but not finished.
	 */
	public static Iterable<Game> getPlayersUnfinishedGames(String login){
		String getPlayersUnfinishedGamesQuery = "SELECT gs FROM GameState gs WHERE gs._isGameOver = false AND EXISTS (FROM gs._players pos " +
		                                         "WHERE pos._playerLogin = :login)";
		Query query = _mgr.createQuery(getPlayersUnfinishedGamesQuery, GameState.class);
		query.setParameter("login", login);
		List<GameState> unFinishedGames;
		try {
			unFinishedGames = query.getResultList();
		} catch (Exception e) {
			unFinishedGames = new ArrayList<>();
		}
		List<Game> convertedGames = new ArrayList<>();
		for (GameState gameState : unFinishedGames) {
			convertedGames.add(new Game(gameState));
		}
		return convertedGames;
	}

	/**
	 * Finds games that have not started yet.
	 * @return Iterable of Games that have not started.
	 */
	public static Iterable<Game> getOpenGames(String login)
	{
		String getOpenGamesQuery = "SELECT g FROM GameState g WHERE g._isGameStarted = ?1 AND g" +
		                              "._isGameOver = ?2 AND NOT EXISTS(FROM g._players pos WHERE pos._playerLogin = :login)";
		Query query = _mgr.createQuery(getOpenGamesQuery, GameState.class);
		query.setParameter(1, false);
		query.setParameter(2, false);
		query.setParameter("login", login);
		List<GameState> openGames;
		try {
			openGames = query.getResultList();
		} catch (Exception e) {
			openGames = new ArrayList<>();
		}
		List<Game> convertedGames = new ArrayList<>();
		for (GameState gameState : openGames) {
			convertedGames.add(new Game(gameState));
		}
		return convertedGames;
	}

	public static Iterable<Game> getGamesByPlayer(String login)
	{
		String getGamesByPlayerQuery = "SELECT gs FROM GameState gs WHERE EXISTS (FROM gs._players pos WHERE pos._playerLogin = :login)";
		Query query = _mgr.createQuery(getGamesByPlayerQuery, GameState.class);
		query.setParameter("login", login);
		List<GameState> finishedGames;
		try {
			finishedGames = query.getResultList();
		} catch (Exception e) {
			finishedGames = new ArrayList<>();
		}
		List<Game> convertedGames = new ArrayList<>();
		for (GameState gameState : finishedGames) {
			convertedGames.add(new Game(gameState));
		}
		return convertedGames;
	}
}
