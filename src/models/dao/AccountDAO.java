package models.dao;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.account.AccountList;
import com.stormpath.sdk.application.Application;
import com.stormpath.sdk.application.ApplicationList;
import com.stormpath.sdk.application.Applications;
import com.stormpath.sdk.client.Client;
import com.stormpath.shiro.realm.ApplicationRealm;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.apache.shiro.subject.Subject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by Tim on 12/12/14.
 */
public final class AccountDAO
{
	private static final Pattern USERNAME_VALIDATION = Pattern.compile("^([A-Za-z]+[A-Za-z0-9_!@#$%^&*]{0,49})$");
	private static final Pattern PASSWORD_VALIDATION = Pattern.compile("^((?=.*\\d)(?=.*[A-Z])(?=.*[a-z])" +
	                                                                  "[A-Za-z0-9_!@#$%^&*]{3,50})$");

	private AccountDAO()
	{
		//none
	}
	/**
	 * Gets the username of the authenticated or remembered user.
	 * @return the username of the authenticated or remembered user.
	 */
	public static String getActiveUsername()
	{
		String username = null;
		if(SecurityUtils.getSubject().isAuthenticated() || SecurityUtils.getSubject().isRemembered())
		{
		  ApplicationRealm applicationRealm = (ApplicationRealm) 
				  ((RealmSecurityManager) 
						  SecurityUtils.getSecurityManager()).getRealms().iterator().next();
		  Client client = applicationRealm.getClient();
		  String accountHref = (String) SecurityUtils.getSubject().getPrincipal();
		  Account account = client.getResource(accountHref, Account.class);
		  username = account.getUsername();
		}
		return username;
	}

	/**
	 * Creates an account in the account store.
	 * @param desiredUsername the desiredUsername of the account.
	 * @param desiredPassword the desiredPassword of the account.
	 * @throws IllegalArgumentException if desiredUsername or password do not meet validation requirements.
	 */
	public static void createAccount(String desiredUsername, String desiredPassword)
	{
		validateCredentials(desiredUsername, desiredPassword);

		if(usernameExists(desiredUsername))
		{
			throw new IllegalArgumentException("Username already taken");
		}

		Client client = getClient();

		Account account = client.instantiate(Account.class);

		//Set properties
		account.setGivenName(desiredUsername); //required field for Stormpath
		account.setSurname(desiredUsername); //required field for Stormpath
		account.setEmail(desiredUsername+"@example.com"); //required field for Stormpath
		account.setUsername(desiredUsername); //field we actually want.
		account.setPassword(desiredPassword); //field we actually want.

		Application application = getApplication();

		//Create the account
		application.createAccount(account);
	}

	/**
	 * Authenticates account given username and password.
	 * @param username username of the account.
	 * @param password password of the account.
	 * @param host host of the end-user
	 */
	public static void authenticateAccount(String username, String password, String host)
	{
		validateCredentials(username, password);

		UsernamePasswordToken token = new UsernamePasswordToken(username, password, host);
		Subject currentUser = SecurityUtils.getSubject();
		currentUser.login(token);
	}

	/**
	 * Checks account store to see if username already exists.
	 * @param username username to check account store for.
	 * @return true if exists, otherwise false
	 */
	public static boolean usernameExists(String username)
	{
		validateUsername(username);

		//Query the account store for username
		Map<String, Object> queryParams = new HashMap<String, Object>();
		queryParams.put("username", username);
		AccountList accounts = getApplication().getAccounts(queryParams);

		//Check to see if username is in the account store.
		Iterator<Account> iterator = accounts.iterator();
		while(iterator.hasNext())
		{
			if(iterator.next().getUsername().trim().equalsIgnoreCase(username.trim()))
			{
				return true;
			}
		}

		return false;
	}

	public static Pattern getPasswordValidation()
	{
		return PASSWORD_VALIDATION;
	}

	public static Pattern getUsernameValidation()
	{
		return USERNAME_VALIDATION;
	}

	private static Application getApplication()
	{
		Client client = getClient();
		ApplicationList applications = client.getApplications(Applications.where(Applications.name().eqIgnoreCase
				("SecureScrabble")));
		Application application = applications.iterator().next();

		return application;
	}

	private static Client getClient()
	{
		ApplicationRealm applicationRealm = (ApplicationRealm)((RealmSecurityManager) SecurityUtils
				.getSecurityManager()).getRealms().iterator().next();
		Client client = applicationRealm.getClient();

		return client;
	}

	private static void validateCredentials(String username, String password)
	{
		validateUsername(username);
		validatePassword(password);
	}

	private static void validateUsername(String username)
	{
		if(!USERNAME_VALIDATION.matcher(username).matches())
		{
			throw new IllegalArgumentException("Username does not meet validation requirements.");
		}
	}

	private static void validatePassword(String password)
	{
		if(!PASSWORD_VALIDATION.matcher(password).matches())
		{
			throw new IllegalArgumentException("Password does not meet validation requirements.");
		}
	}
}
