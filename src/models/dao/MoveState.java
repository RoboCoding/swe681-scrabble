package models.dao;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * MoveState
 * Persistence class representing a Move object.
 */
@Entity
@Table(name = "GameMoves")
public class MoveState implements Serializable
{
	// The globally unique move ID for this move.
	@Id
	@GeneratedValue
	@Column(name = "UniqueMoveID")
	private int _uniqueMoveID;
	
	// The "game-unique" move ID for this move.
	@Column(name = "MoveID")
	private int _moveID;
	
	// The name of the player that made this move.
	@Column(name = "PlayerID")
	private String _player;
	
	// The text of the move
	@Column(name = "Move")
	private String _moveText;
	
	// The score of the move
	@Column(name = "Score")
	private int _score;
	
	// The time when the move was made
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MoveTimestamp")
	private Date _timeStamp;
	
	/**
	 * Gets the globally unique ID for this move.
	 * @return A globally unique ID for this move.
	 */
	public int getUniqueId()
	{
		return _uniqueMoveID;
	}
	
	/**
	 * Sets the globally unique ID for this move.
	 * @param id The globally unique ID for this move.
	 */
	public void setUniqueId(int id)
	{
		_uniqueMoveID = id;
	}
	
	/**
	 * Gets the "game-unique" ID for this move.
	 * @return The "game-unique" ID for this move.
	 */
	public int getMoveId()
	{
		return _moveID;
	}
	
	/**
	 * Sets the "game-unique" ID for this move.
	 * @param id The "game-unique" ID for this move.
	 */
	public void setMoveId(int id)
	{
		_moveID = id;
	}
	
	/**
	 * Gets the name of the player that made this move.
	 * @return The name of the player that made this move.
	 */
	public String getPlayer()
	{
		return _player;
	}
	
	/**
	 * Sets the name of the player that made this move.
	 * @param player The name of the player that made this move.
	 */
	public void setPlayer(String player)
	{
		_player = player;
	}
	
	/**
	 * Gets the text of this move
	 * @return Textual representation of this move
	 */
	public String getMoveText()
	{
		return _moveText;
	}
	
	/**
	 * Sets the text of this move
	 * @param moveText Textual representation of this move
	 */
	public void setMoveText(String moveText)
	{
		_moveText = moveText;
	}
	
	/**
	 * Gets the score of this move
	 * @return The score of this move
	 */
	public int getScore()
	{
		return _score;
	}
	
	/**
	 * Sets the score of this move
	 * @param score The score of this move
	 */
	public void setScore(int score)
	{
		_score = score;
	}
	
	/**
	 * Gets the time when this move was made
	 * @return The Date representing when this move was made
	 */
	public Date getTimestamp()
	{
		return _timeStamp;
	}
	
	/**
	 * Sets the time when this move was made
	 * @param timestamp The Date representing when this move was made
	 */
	public void setTimestamp(Date timestamp)
	{
		_timeStamp = timestamp;
	}
}
