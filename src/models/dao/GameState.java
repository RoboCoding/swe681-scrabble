package models.dao;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

/**
 * GameState
 * Persistence class representing a Game object.
 */
@Entity
@Table(name = "GameData")
public class GameState implements Serializable
{
	// Globally unique game ID
	@Id
	@GeneratedValue
	@Column(name = "GameID")
	private int _gameID;
	
	// The order of letters in the letter pouch for this game
	@Column(name = "LetterPouch")
	private String _letterPouch;
	
	// Whether this game has started
	@Column(name = "GameStarted")
	private boolean _isGameStarted;
	
	// Whether this game has ended
	@Column(name = "GameCompleted")
	private boolean _isGameOver;
	
	// If the game ended, then the name of the player that won the game
	@Column(name = "WinningPlayer")
	private String _winningPlayer;
	
	// Time when the game was made
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "GameTimestamp")
	private Date _startingTimestamp;
	
	// The moves made so far in this game
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "GameID", referencedColumnName = "GameID")
	private final Set<MoveState> _moves = new HashSet<>();
	
	// The players participating in this game
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "GameID", referencedColumnName = "GameID")
	private final List<PlayerOrderState> _players = new ArrayList<>();

	/**
	 * Gets the globally unique ID for this game
	 * @return The ID of this game
	 */
	public int getGameId()
	{
		return _gameID;
	}
	
	/**
	 * Sets the globally unique ID for this game
	 * @param id The ID of this game
	 */
	public void setGameId(int id)
	{
		_gameID = id;
	}
	
	/**
	 * Gets the ordering of letters to use for this game
	 * @return The letters to use for this game
	 */
	public String getLetterPouch()
	{
		return _letterPouch;
	}
	
	/**
	 * Sets the ordering of letters to use for this game
	 * @param letterPouch The letters to use for this game
	 */
	public void setLetterPouch(String letterPouch)
	{
		_letterPouch = letterPouch;
	}
	
	/**
	 * Gets whether this game has started
	 * @return True if the game has started. Else, false.
	 */
	public boolean getIsGameStarted()
	{
		return _isGameStarted;
	}
	
	/**
	 * Sets whether the game has started
	 * @param isGameStarted True if the game has started. Else, false.
	 */
	public void setIsGameStarted(boolean isGameStarted)
	{
		_isGameStarted = isGameStarted;
	}

	/**
	 * Gets whether the game has ended
	 * @return True if the game has ended. Else, false.
	 */
	public boolean getIsGameOver()
	{
		return _isGameOver;
	}
	
	/**
	 * Sets whether the game has ended
	 * @param isGameOver True if the game has ended. Else, false.
	 */
	public void setIsGameOver(boolean isGameOver)
	{
		_isGameOver = isGameOver;
	}
	
	/**
	 * Gets the moves made so far in the game
	 * @return Iterable of MoveState objects representing the moves to date
	 */
	public Iterable<MoveState> getMoves()
	{
		return _moves;
	}
	
	/**
	 * Sets the moves made so far in the game
	 * @param moves Iterable of MoveState objects representing the moves to date
	 */
	public void setMoves(Iterable<MoveState> moves)
	{
		_moves.clear();
		for (MoveState move : moves) {
			_moves.add(move);
		}
	}
	
	/**
	 * Gets the players participating in this game
	 * @return Iterable of PlayerOrderState objects representing the players in the game
	 */
	public List<PlayerOrderState> getPlayers()
	{
		return _players;
	}
	
	/**
	 * Sets the players participating in this game
	 * @param players Iterable of PlayerOrderState objects representing the players in the game.
	 */
	public void setPlayers(List<PlayerOrderState> players)
	{
		_players.clear();
		for (PlayerOrderState player : players) {
			_players.add(player);
		}
	}
	
	/**
	 * If the game is over, gets the player that won the game.
	 * @return If the game is over, then this returns the name of the player 
	 * that won the game. Else, return value is undetermined.
	 */
	public String getWinningPlayer()
	{
		return _winningPlayer;
	}
	
	/**
	 * Sets the player that won the game
	 * @param winner The name of the player that won the game
	 */
	public void setWinningPlayer(String winner)
	{
		_winningPlayer = winner;
	}
	
	/**
	 * Gets the timestamp for the start of the game
	 * @return Date representing the start of the game
	 */
	public Date getStartingTimestamp()
	{
		return _startingTimestamp;
	}
	
	/**
	 * Sets the timestamp for the start of the game
	 * @param timestamp Date representing the start of the game
	 */
	public void setStartingTimestamp(Date timestamp)
	{
		_startingTimestamp = timestamp;
	}
}
