<%@ include file="header.jsp"%>
<div class="container">
  <shiro:user>
    <div class="page-header center">
      <h1 class="text-center">Completed Games</h1>
    </div>
		<table class="table striped-table table-hover">
			<thead>
				<tr>
					<td>Game ID</td>
					<td>Start Time</td>
					<td>Number of Players</td>
					<td>Winning Player</td>
					<td>View Details</td>
				</tr>
			</thead>
			<tbody>
				<s:iterator value="completedGames">
					<s:url id="detailView" action="gameDetails">
						<s:param name="gameId">${gameId}</s:param>
					</s:url>
					<tr>
						<td>${gameId}</td>
						<td>${gameStartTimestamp}</td>
						<td>${numPlayers}</td>
						<td>${winningPlayer}</td>
						<td><s:a href="%{detailView}">Details</s:a></td>
					</tr>
				</s:iterator>
			</tbody>
		</table>
  </shiro:user>
</div>
<%@ include file="footer.jsp"%>