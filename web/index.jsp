<%@ include file="header.jsp"%>
<div class="container">
<shiro:guest>
  <div class="page-header center"><h1 class="text-center">Welcome to SecureScrabble</h1></div>

  <div class="row">
    <div class="col-xs-5 col-xs-offset-1">
      <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">Register</h3>
        </div>
        <div class="panel-body">
          <form role="form" id="registerForm" action="register.action" method="post" autocomplete="on">

            <div class="form-group">
              <label for="desiredUsername">Username*:</label>
              <input type="text" class="form-control" id="desiredUsername" name="desiredUsername" required />
              <s:fielderror fieldName="desiredUsername" cssClass="text-danger bg-danger" />
            </div>

            <div class="row">
              <div class="col-xs-6">
                <div class="form-group">
                  <label for="desiredPassword">Password*:</label>
                  <input type="password" class="form-control" id="desiredPassword" name="desiredPassword" required />
                  <s:fielderror fieldName="desiredPassword" cssClass="text-danger bg-danger" />
                </div>
              </div>

              <div class="col-xs-6">
                <div class="form-group">
                  <label for="confirmDesiredPassword">Confirm Password*:</label>
                  <input type="password" class="form-control" id="confirmDesiredPassword" name="confirmDesiredPassword" required />
                  <s:fielderror fieldName="confirmDesiredPassword" cssClass="text-danger bg-danger" />
                </div>
              </div>
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-primary">Register</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <div class="col-xs-5">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Login</h3>
        </div>
        <div class="panel-body">
          <form role="form" id="loginForm" action="" method="post" accept-charset="UTF-8" autocomplete="on">

            <div class="form-group">
              <label for="username">Username:</label>
              <input type="text" class="form-control" id="username" name="username" autofocus required />
            </div>

            <div class="form-group">
              <label for="password">Password:</label>
              <input type="password" class="form-control" id="password" name="password" required />
            </div>

            <div class="form-group">
              <button type="submit" class="btn btn-default">Login</button>
              <div class="checkbox-inline">
                <label><input type="checkbox" id="rememberMe" name="rememberMe">Remember Me</label>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</shiro:guest>
<shiro:user>
  <div class="page-header center">
    <h1 class="text-center">Welcome Back <s:property value="username" /></h1>
        <h1 class="text-center"><small>Please go to your
      <a href="<s:url action="home" />">home</a> or <a href="<c:url value="/logout"/>">Logout</a>.</small>
    </h1>
  </div>
</shiro:user>
</div>
<%@ include file="footer.jsp" %>