<%@ include file="header.jsp"%>
<div class="container">
<shiro:user>
  <script type="text/javascript">
  $(function() {
  setInterval(function() {
  window.location.reload();
  }, 15000);
  })
  </script>
  <div class="page-header center">
    <h1 class="text-center">Welcome Back <s:property value="username" /></h1>
  </div>
  <div class="row">
    <ul class="list-inline text-center">
      <li>
        <s:form action="newGame">
          <s:hidden name="creator" value="%{username}" />
          <s:submit value="New Game" cssClass="btn btn-primary" type="button" />
        </s:form>
      </li>
    </ul>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <s:if test="%{!inputErrors.isEmpty()}">
        <ul class="bg-danger">
          <s:iterator value="%{inputErrors}" var="error">
            <li><s:property value="%{#error}" /></li>
          </s:iterator>
        </ul>
      </s:if>
    </div>
  </div>
  <s:if test="%{!unfinishedGames.isEmpty()}">
  <div class="row">
    <div class="col-xs-12">
      <h2 class="text-center">Unfinished Games</h2>
      <table class="table striped-table table-hover">
      <thead>
        <tr>
          <td>Game ID</td>
          <td>Join</td>
        </tr>
      </thead>
      <s:iterator value="%{unfinishedGames}" var="game" >
        <tr>
          <td>${game.gameId}</td>
          <td><s:form action="joinGame">
              <s:hidden name="gameID" value="%{#game.gameId}" />
              <s:submit value="Join Game" cssClass="btn btn-success" type="button" />
            </s:form></td>
        </tr>
      </s:iterator>
      </table>
    </div>
  </div>
  </s:if>
  <s:if test="%{!openGames.isEmpty()}">
  <div class="row">
    <div class="col-xs-12">
      <h2 class="text-center">Open Games</h2>
      <table class="table striped-table table-hover">
        <thead>
        <tr>
          <td>Game ID</td>
          <td>Join</td>
        </tr>
        </thead>
        <s:iterator value="%{openGames}" var="openGame" >
          <tr>
            <td>${openGame.gameId}</td>
            <td><s:form action="joinGame">
              <s:hidden name="gameID" value="%{#openGame.gameId}" />
              <s:submit value="Join Game" cssClass="btn btn-success" type="button" />
            </s:form></td>
          </tr>
        </s:iterator>
      </table>
    </div>
  </div>
 </s:if>
</shiro:user>
 </div>
<%@ include file="footer.jsp"%>