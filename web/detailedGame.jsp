<%@ include file="header.jsp"%>
<div class="container">
  <shiro:user>
    <div class="page-header center">
      <h1 class="text-center">Detailed Game View</h1>
    </div>
		<h2 class="text-center">Game Details</h2>
		<table class="table striped-table table-hover">
			<thead>
				<tr>
					<td>Game ID</td>
					<td>Start Time</td>
					<td>Number of Players</td>
					<td>Winning Player</td>
				</tr>
			</thead>
			<tbody>
					<tr>
						<td>${detailedGame.gameId}</td>
						<td>${detailedGame.gameStartTimestamp}</td>
						<td>${detailedGame.numPlayers}</td>
						<td>${detailedGame.winningPlayer}</td>
					</tr>
			</tbody>
		</table>
		<h2 class="text-center">Score Details</h2>
		<table class="table striped-table table-hover">
			<thead>
				<tr>
					<td>Player Name</td>
					<td>Score</td>
				</tr>
			</thead>
			<tbody>
				<s:iterator value="detailedGame.players">
					<tr>
						<td>${playerName}</td>
						<td>${playerScore}</td>
					</tr>
				</s:iterator>
			</tbody>
		</table>
		<h2 class="text-center">Move History</h2>
		<table class="table striped-table table-hover">
			<thead>
				<tr>
					<td>Player Name</td>
					<td>Move</td>
					<td>Score</td>
				</tr>
			</thead>
			<tbody>
				<s:iterator value="detailedGame.moves">
					<tr>
						<td>${player}</td>
						<td>${move}</td>
						<td>${score}</td>
					</tr>
				</s:iterator>
			</tbody>
		</table>
		<p class="text-center"><a href="javascript:history.go(-1)">Click here to go back</a></p>
  </shiro:user>
</div>
<%@ include file="footer.jsp"%>