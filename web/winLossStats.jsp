<%@ include file="header.jsp"%>
<div class="container">
  <shiro:user>
    <div class="page-header center">
      <h1 class="text-center">Win/Loss Statistics</h1>
    </div>
  </shiro:user>
	<table class="table">
		<thead>
			<tr>
				<td>User</td>
				<td>Wins</td>
				<td>Losses</td>
				<td>Percentage</td>
			</tr>
		</thead>
		<tbody>
		<s:iterator value="stats" var="stat" >
			<tr>
				<td>${stat.username}</td>
				<td>${stat.gamesWon}</td>
				<td>${stat.gamesLost}</td>
				<td>${stat.winPercentage}</td>
			</tr>
		</s:iterator>
		</tbody>
	</table>
</div>
<%@ include file="footer.jsp"%>