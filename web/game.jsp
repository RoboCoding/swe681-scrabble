<%@ include file="header.jsp"%>
<script type="text/javascript">
  $(function() {
    <s:iterator value="playerView.playedLetters" var="item">
      addLetter("${item.letterPosition.x}", "${item.letterPosition.y}", "${item.letter}");
    </s:iterator>
  });
  $(function() {
    <s:if test="%{(playerView.isMyTurn == false && !playerView.isGameOver) || !playerView.isGameStarted}">
     setInterval(function() {
      window.location.reload();
      }, 5000);
    </s:if>
  })

</script>

  <form role="form" id="makeMove" action="makeMove.action" method="post" autocomplete="off">
    <input type="text" hidden="true" name="gameID" value="${gameID}" />
<div class="container">
  <s:if test="%{playerView.isGameOver}">
    <div class = "page-header">
      <h1>The game is over.</h1>
      <h1><small>${playerView.winner} won the game with a score of ${playerView.winningScore}</small></h1>
    </div>
  </s:if>

  <s:if test="%{!playerView.isGameStarted}">
    <div class="page-header center"><h1>
      Waiting on players... try <a href="<s:url action="playGame">
      <s:param name="username">${username}</s:param>
      <s:param name="gameID">${gameID}</s:param>
      </s:url>">refreshing.</a></h1>
    </div>
  </s:if>

  <s:if test="%{playerView.playerOnClock.equalsIgnoreCase(username)}">
    <div class = "page-header">
      <h1>Your turn.</h1>
    </div>
  </s:if>

  <s:if test="%{!inputErrors.isEmpty()}">
      <ul class="bg-danger">
      <s:iterator value="%{inputErrors}" var="error">
        <li>${error}</li>
      </s:iterator>
      </ul>
  </s:if>

  <table class="table table-condensed">
    <tr>
      <th colspan="2">Current Standings</th>
    </tr>
    <s:iterator value="%{playerView.allPlayers}" var="player">
      <s:if test="%{playerView.playerOnClock.equalsIgnoreCase(#player.playerName)}">
        <tr class="success">
      </s:if>
      <s:else>
        <tr>
      </s:else>
        <td><s:property value="playerName" /></td>
        <td><s:property value="score" /></td>
    </tr>
    </s:iterator>
  </table>
    <!--Scrabble Board-->
    <div class="row">
      <div class="row">
          <div class="col-xs-12">
            <table class="gameBoard">
              <tr>
                <td class="triple-word" id="a1">
                  <span><br></span><input type="radio" name="startingCoordinate" value="a1" />
                </td>
                <td class="blank" id="b1">
                  <span><br></span><input type="radio" name="startingCoordinate" value="b1" />
                </td>
                <td class="blank" id="c1">
                  <span><br></span><input type="radio" name="startingCoordinate" value="c1" />
                </td>
                <td class="double-letter" id="d1">
                  <span><br></span><input type="radio" name="startingCoordinate" value="d1" />
                </td>
                <td class="blank" id="e1">
                  <span><br></span><input type="radio" name="startingCoordinate" value="e1" />
                </td>
                <td class="blank" id="f1">
                  <span><br></span><input type="radio" name="startingCoordinate" value="f1" />
                </td>
                <td class="blank" id="g1">
                  <span><br></span><input type="radio" name="startingCoordinate" value="g1" />
                </td>
                <td class="triple-word" id="h1">
                  <span><br></span><input type="radio" name="startingCoordinate" value="h1" />
                </td>
                <td class="blank" id="i1">
                  <span><br></span><input type="radio" name="startingCoordinate" value="i1" />
                </td>
                <td class="blank" id="j1">
                  <span><br></span><input type="radio" name="startingCoordinate" value="j1" />
                </td>
                <td class="blank" id="k1">
                  <span><br></span><input type="radio" name="startingCoordinate" value="k1" />
                </td>
                <td class="double-letter" id="l1">
                  <span><br></span><input type="radio" name="startingCoordinate" value="l1" />
                </td>
                <td class="blank" id="m1">
                  <span><br></span><input type="radio" name="startingCoordinate" value="m1" />
                </td>
                <td class="blank" id="n1">
                  <span><br></span><input type="radio" name="startingCoordinate" value="n1" />
                </td>
                <td class="triple-word" id="o1">
                  <span><br></span><input type="radio" name="startingCoordinate" value="o1" />
                </td>
              </tr>
              <tr>
                <td class="blank" id="a2">
                  <span><br></span><input type="radio" name="startingCoordinate" value="a2" />
                </td>
                <td class="double-word" id="b2">
                  <span><br></span><input type="radio" name="startingCoordinate" value="b2" />
                </td>
                <td class="blank" id="c2">
                  <span><br></span><input type="radio" name="startingCoordinate" value="c2" />
                </td>
                <td class="blank" id="d2">
                  <span><br></span><input type="radio" name="startingCoordinate" value="d2" />
                </td>
                <td class="blank" id="e2">
                  <span><br></span><input type="radio" name="startingCoordinate" value="e2" />
                </td>
                <td class="triple-letter" id="f2">
                  <span><br></span><input type="radio" name="startingCoordinate" value="f2" />
                </td>
                <td class="blank" id="g2">
                  <span><br></span><input type="radio" name="startingCoordinate" value="g2" />
                </td>
                <td class="blank" id="h2">
                  <span><br></span><input type="radio" name="startingCoordinate" value="h2" />
                </td>
                <td class="blank" id="i2">
                  <span><br></span><input type="radio" name="startingCoordinate" value="i2" />
                </td>
                <td class="triple-letter" id="j2">
                  <span><br></span><input type="radio" name="startingCoordinate" value="j2" />
                </td>
                <td class="blank" id="k2">
                  <span><br></span><input type="radio" name="startingCoordinate" value="k2" />
                </td>
                <td class="blank" id="l2">
                  <span><br></span><input type="radio" name="startingCoordinate" value="l2" />
                </td>
                <td class="blank" id="m2">
                  <span><br></span><input type="radio" name="startingCoordinate" value="m2" />
                </td>
                <td class="double-word" id="n2">
                  <span><br></span><input type="radio" name="startingCoordinate" value="n2" />
                </td>
                <td class="blank" id="o2">
                  <span><br></span><input type="radio" name="startingCoordinate" value="o2" />
                </td>
              </tr>
              <tr>
                <td class="blank" id="a3">
                  <span><br></span><input type="radio" name="startingCoordinate" value="a3" />
                </td>
                <td class="blank" id="b3">
                  <span><br></span><input type="radio" name="startingCoordinate" value="b3" />
                </td>
                <td class="double-word" id="c3">
                  <span><br></span><input type="radio" name="startingCoordinate" value="c3" />
                </td>
                <td class="blank" id="d3">
                  <span><br></span><input type="radio" name="startingCoordinate" value="d3" />
                </td>
                <td class="blank" id="e3">
                  <span><br></span><input type="radio" name="startingCoordinate" value="e3" />
                </td>
                <td class="blank" id="f3">
                  <span><br></span><input type="radio" name="startingCoordinate" value="f3" />
                </td>
                <td class="double-letter" id="g3">
                  <span><br></span><input type="radio" name="startingCoordinate" value="g3" />
                </td>
                <td class="blank" id="h3">
                  <span><br></span><input type="radio" name="startingCoordinate" value="h3" />
                </td>
                <td class="double-letter" id="i3">
                  <span><br></span><input type="radio" name="startingCoordinate" value="i3" />
                </td>
                <td class="blank" id="j3">
                  <span><br></span><input type="radio" name="startingCoordinate" value="j3" />
                </td>
                <td class="blank" id="k3">
                  <span><br></span><input type="radio" name="startingCoordinate" value="k3" />
                </td>
                <td class="blank" id="l3">
                  <span><br></span><input type="radio" name="startingCoordinate" value="l3" />
                </td>
                <td class="double-word" id="m3">
                  <span><br></span><input type="radio" name="startingCoordinate" value="m3" />
                </td>
                <td class="blank" id="n3">
                  <span><br></span><input type="radio" name="startingCoordinate" value="n3" />
                </td>
                <td class="blank" id="o3">
                  <span><br></span><input type="radio" name="startingCoordinate" value="o3" />
                </td>
              </tr>
              <tr>
                <td class="double-letter" id="a4">
                  <span><br></span><input type="radio" name="startingCoordinate" value="a4" />
                </td>
                <td class="blank" id="b4">
                  <span><br></span><input type="radio" name="startingCoordinate" value="b4" />
                </td>
                <td class="blank" id="c4">
                  <span><br></span><input type="radio" name="startingCoordinate" value="c4" />
                </td>
                <td class="double-word" id="d4">
                  <span><br></span><input type="radio" name="startingCoordinate" value="d4" />
                </td>
                <td class="blank" id="e4">
                  <span><br></span><input type="radio" name="startingCoordinate" value="e4" />
                </td>
                <td class="blank" id="f4">
                  <span><br></span><input type="radio" name="startingCoordinate" value="f4" />
                </td>
                <td class="blank" id="g4">
                  <span><br></span><input type="radio" name="startingCoordinate" value="g4" />
                </td>
                <td class="double-letter" id="h4">
                  <span><br></span><input type="radio" name="startingCoordinate" value="h4" />
                </td>
                <td class="blank" id="i4">
                  <span><br></span><input type="radio" name="startingCoordinate" value="i4" />
                </td>
                <td class="blank" id="j4">
                  <span><br></span><input type="radio" name="startingCoordinate" value="j4" />
                </td>
                <td class="blank" id="k4">
                  <span><br></span><input type="radio" name="startingCoordinate" value="k4" />
                </td>
                <td class="double-word" id="l4">
                  <span><br></span><input type="radio" name="startingCoordinate" value="l4" />
                </td>
                <td class="blank" id="m4">
                  <span><br></span><input type="radio" name="startingCoordinate" value="m4" />
                </td>
                <td class="blank" id="n4">
                  <span><br></span><input type="radio" name="startingCoordinate" value="n4" />
                </td>
                <td class="double-letter" id="o4">
                  <span><br></span><input type="radio" name="startingCoordinate" value="o4" />
                </td>
              </tr>
              <tr>
                <td class="blank" id="a5">
                  <span><br></span><input type="radio" name="startingCoordinate" value="a5" />
                </td>
                <td class="blank" id="b5">
                  <span><br></span><input type="radio" name="startingCoordinate" value="b5" />
                </td>
                <td class="blank" id="c5">
                  <span><br></span><input type="radio" name="startingCoordinate" value="c5" />
                </td>
                <td class="blank" id="d5">
                  <span><br></span><input type="radio" name="startingCoordinate" value="d5" />
                </td>
                <td class="double-word" id="e5">
                  <span><br></span><input type="radio" name="startingCoordinate" value="e5" />
                </td>
                <td class="blank" id="f5">
                  <span><br></span><input type="radio" name="startingCoordinate" value="f5" />
                </td>
                <td class="blank" id="g5">
                  <span><br></span><input type="radio" name="startingCoordinate" value="g5" />
                </td>
                <td class="blank" id="h5">
                  <span><br></span><input type="radio" name="startingCoordinate" value="h5" />
                </td>
                <td class="blank" id="i5">
                  <span><br></span><input type="radio" name="startingCoordinate" value="i5" />
                </td>
                <td class="blank" id="j5">
                  <span><br></span><input type="radio" name="startingCoordinate" value="j5" />
                </td>
                <td class="double-word" id="k5">
                  <span><br></span><input type="radio" name="startingCoordinate" value="k5" />
                </td>
                <td class="blank" id="l5">
                  <span><br></span><input type="radio" name="startingCoordinate" value="l5" />
                </td>
                <td class="blank" id="m5">
                  <span><br></span><input type="radio" name="startingCoordinate" value="m5" />
                </td>
                <td class="blank" id="n5">
                  <span><br></span><input type="radio" name="startingCoordinate" value="n5" />
                </td>
                <td class="blank" id="o5">
                  <span><br></span><input type="radio" name="startingCoordinate" value="o5" />
                </td>
              </tr>
              <tr>
                <td class="blank" id="a6">
                  <span><br></span><input type="radio" name="startingCoordinate" value="a6" />
                </td>
                <td class="triple-word" id="b6">
                  <span><br></span><input type="radio" name="startingCoordinate" value="b6" />
                </td>
                <td class="blank" id="c6">
                  <span><br></span><input type="radio" name="startingCoordinate" value="c6" />
                </td>
                <td class="blank" id="d6">
                  <span><br></span><input type="radio" name="startingCoordinate" value="d6" />
                </td>
                <td class="blank" id="e6">
                  <span><br></span><input type="radio" name="startingCoordinate" value="e6" />
                </td>
                <td class="triple-word" id="f6">
                  <span><br></span><input type="radio" name="startingCoordinate" value="f6" />
                </td>
                <td class="blank" id="g6">
                  <span><br></span><input type="radio" name="startingCoordinate" value="g6" />
                </td>
                <td class="blank" id="h6">
                  <span><br></span><input type="radio" name="startingCoordinate" value="h6" />
                </td>
                <td class="blank" id="i6">
                  <span><br></span><input type="radio" name="startingCoordinate" value="i6" />
                </td>
                <td class="triple-word" id="j6">
                  <span><br></span><input type="radio" name="startingCoordinate" value="j6" />
                </td>
                <td class="blank" id="k6">
                  <span><br></span><input type="radio" name="startingCoordinate" value="k6" />
                </td>
                <td class="blank" id="l6">
                  <span><br></span><input type="radio" name="startingCoordinate" value="l6" />
                </td>
                <td class="blank" id="m6">
                  <span><br></span><input type="radio" name="startingCoordinate" value="m6" />
                </td>
                <td class="triple-word" id="n6">
                  <span><br></span><input type="radio" name="startingCoordinate" value="n6" />
                </td>
                <td class="blank" id="o6">
                  <span><br></span><input type="radio" name="startingCoordinate" value="o6" />
                </td>
              </tr>
              <tr>
                <td class="blank" id="a7">
                  <span><br></span><input type="radio" name="startingCoordinate" value="a7" />
                </td>
                <td class="blank" id="b7">
                  <span><br></span><input type="radio" name="startingCoordinate" value="b7" />
                </td>
                <td class="double-word" id="c7">
                  <span><br></span><input type="radio" name="startingCoordinate" value="c7" />
                </td>
                <td class="blank" id="d7">
                  <span><br></span><input type="radio" name="startingCoordinate" value="d7" />
                </td>
                <td class="blank" id="e7">
                  <span><br></span><input type="radio" name="startingCoordinate" value="e7" />
                </td>
                <td class="blank" id="f7">
                  <span><br></span><input type="radio" name="startingCoordinate" value="f7" />
                </td>
                <td class="double-word" id="g7">
                  <span><br></span><input type="radio" name="startingCoordinate" value="g7" />
                </td>
                <td class="blank" id="h7">
                  <span><br></span><input type="radio" name="startingCoordinate" value="h7" />
                </td>
                <td class="double-word" id="i7">
                  <span><br></span><input type="radio" name="startingCoordinate" value="i7" />
                </td>
                <td class="blank" id="j7">
                  <span><br></span><input type="radio" name="startingCoordinate" value="j7" />
                </td>
                <td class="blank" id="k7">
                  <span><br></span><input type="radio" name="startingCoordinate" value="k7" />
                </td>
                <td class="blank" id="l7">
                  <span><br></span><input type="radio" name="startingCoordinate" value="l7" />
                </td>
                <td class="double-word" id="m7">
                  <span><br></span><input type="radio" name="startingCoordinate" value="m7" />
                </td>
                <td class="blank" id="n7">
                  <span><br></span><input type="radio" name="startingCoordinate" value="n7" />
                </td>
                <td class="blank" id="o7">
                  <span><br></span><input type="radio" name="startingCoordinate" value="o7" />
                </td>
              </tr>
              <tr>
                <td class="triple-word" id="a8">
                  <span><br></span><input type="radio" name="startingCoordinate" value="a8" />
                </td>
                <td class="blank" id="b8">
                  <span><br></span><input type="radio" name="startingCoordinate" value="b8" />
                </td>
                <td class="blank" id="c8">
                  <span><br></span><input type="radio" name="startingCoordinate" value="c8" />
                </td>
                <td class="double-letter" id="d8">
                  <span><br></span><input type="radio" name="startingCoordinate" value="d8" />
                </td>
                <td class="blank" id="e8">
                  <span><br></span><input type="radio" name="startingCoordinate" value="e8" />
                </td>
                <td class="blank" id="f8">
                  <span><br></span><input type="radio" name="startingCoordinate" value="f8" />
                </td>
                <td class="blank" id="g8">
                  <span><br></span><input type="radio" name="startingCoordinate" value="g8" />
                </td>
                <td class="star" id="h8">
                  <span><br></span><input type="radio" name="startingCoordinate" value="h8" />
                </td>
                <td class="blank" id="i8">
                  <span><br></span><input type="radio" name="startingCoordinate" value="i8" />
                </td>
                <td class="blank" id="j8">
                  <span><br></span><input type="radio" name="startingCoordinate" value="j8" />
                </td>
                <td class="blank" id="k8">
                  <span><br></span><input type="radio" name="startingCoordinate" value="k8" />
                </td>
                <td class="double-letter" id="l8">
                  <span><br></span><input type="radio" name="startingCoordinate" value="l8" />
                </td>
                <td class="blank" id="m8">
                  <span><br></span><input type="radio" name="startingCoordinate" value="m8" />
                </td>
                <td class="blank" id="n8">
                  <span><br></span><input type="radio" name="startingCoordinate" value="n8" />
                </td>
                <td class="triple-word" id="o8">
                  <span><br></span><input type="radio" name="startingCoordinate" value="o8" />
                </td>
              </tr>
              <tr>
                <td class="blank" id="a9">
                  <span><br></span><input type="radio" name="startingCoordinate" value="a9" />
                </td>
                <td class="blank" id="b9">
                  <span><br></span><input type="radio" name="startingCoordinate" value="b9" />
                </td>
                <td class="double-letter" id="c9">
                  <span><br></span><input type="radio" name="startingCoordinate" value="c9" />
                </td>
                <td class="blank" id="d9">
                  <span><br></span><input type="radio" name="startingCoordinate" value="d9" />
                </td>
                <td class="blank" id="e9">
                  <span><br></span><input type="radio" name="startingCoordinate" value="e9" />
                </td>
                <td class="blank" id="f9">
                  <span><br></span><input type="radio" name="startingCoordinate" value="f9" />
                </td>
                <td class="double-letter" id="g9">
                  <span><br></span><input type="radio" name="startingCoordinate" value="g9" />
                </td>
                <td class="blank" id="h9">
                  <span><br></span><input type="radio" name="startingCoordinate" value="h9" />
                </td>
                <td class="double-letter" id="i9">
                  <span><br></span><input type="radio" name="startingCoordinate" value="i9" />
                </td>
                <td class="blank" id="j9">
                  <span><br></span><input type="radio" name="startingCoordinate" value="j9" />
                </td>
                <td class="blank" id="k9">
                  <span><br></span><input type="radio" name="startingCoordinate" value="k9" />
                </td>
                <td class="blank" id="l9">
                  <span><br></span><input type="radio" name="startingCoordinate" value="l9" />
                </td>
                <td class="double-letter" id="m9">
                  <span><br></span><input type="radio" name="startingCoordinate" value="m9" />
                </td>
                <td class="blank" id="n9">
                  <span><br></span><input type="radio" name="startingCoordinate" value="n9" />
                </td>
                <td class="blank" id="o9">
                  <span><br></span><input type="radio" name="startingCoordinate" value="o9" />
                </td>
              </tr>
              <tr>
                <td class="blank" id="a10">
                  <span><br></span><input type="radio" name="startingCoordinate" value="a10" />
                </td>
                <td class="triple-letter" id="b10">
                  <span><br></span><input type="radio" name="startingCoordinate" value="b10" />
                </td>
                <td class="blank" id="c10">
                  <span><br></span><input type="radio" name="startingCoordinate" value="c10" />
                </td>
                <td class="blank" id="d10">
                  <span><br></span><input type="radio" name="startingCoordinate" value="d10" />
                </td>
                <td class="blank" id="e10">
                  <span><br></span><input type="radio" name="startingCoordinate" value="e10" />
                </td>
                <td class="triple-letter" id="f10">
                  <span><br></span><input type="radio" name="startingCoordinate" value="f10" />
                </td>
                <td class="blank" id="g10">
                  <span><br></span><input type="radio" name="startingCoordinate" value="g10" />
                </td>
                <td class="blank" id="h10">
                  <span><br></span><input type="radio" name="startingCoordinate" value="h10" />
                </td>
                <td class="blank" id="i10">
                  <span><br></span><input type="radio" name="startingCoordinate" value="i10" />
                </td>
                <td class="triple-letter" id="j10">
                  <span><br></span><input type="radio" name="startingCoordinate" value="j10" />
                </td>
                <td class="blank" id="k10">
                  <span><br></span><input type="radio" name="startingCoordinate" value="k10" />
                </td>
                <td class="blank" id="l10">
                  <span><br></span><input type="radio" name="startingCoordinate" value="l10" />
                </td>
                <td class="blank" id="m10">
                  <span><br></span><input type="radio" name="startingCoordinate" value="m10" />
                </td>
                <td class="triple-letter" id="n10">
                  <span><br></span><input type="radio" name="startingCoordinate" value="n10" />
                </td>
                <td class="blank" id="o10">
                  <span><br></span><input type="radio" name="startingCoordinate" value="o10" />
                </td>
              </tr>
              <tr>
                <td class="blank" id="a11">
                  <span><br></span><input type="radio" name="startingCoordinate" value="a11" />
                </td>
                <td class="blank" id="b11">
                  <span><br></span><input type="radio" name="startingCoordinate" value="b11" />
                </td>
                <td class="blank" id="c11">
                  <span><br></span><input type="radio" name="startingCoordinate" value="c11" />
                </td>
                <td class="blank" id="d11">
                  <span><br></span><input type="radio" name="startingCoordinate" value="d11" />
                </td>
                <td class="double-word" id="e11">
                  <span><br></span><input type="radio" name="startingCoordinate" value="e11" />
                </td>
                <td class="blank" id="f11">
                  <span><br></span><input type="radio" name="startingCoordinate" value="f11" />
                </td>
                <td class="blank" id="g11">
                  <span><br></span><input type="radio" name="startingCoordinate" value="g11" />
                </td>
                <td class="blank" id="h11">
                  <span><br></span><input type="radio" name="startingCoordinate" value="h11" />
                </td>
                <td class="blank" id="i11">
                  <span><br></span><input type="radio" name="startingCoordinate" value="i11" />
                </td>
                <td class="blank" id="j11">
                  <span><br></span><input type="radio" name="startingCoordinate" value="j11" />
                </td>
                <td class="double-word" id="k11">
                  <span><br></span><input type="radio" name="startingCoordinate" value="k11" />
                </td>
                <td class="blank" id="l11">
                  <span><br></span><input type="radio" name="startingCoordinate" value="l11" />
                </td>
                <td class="blank" id="m11">
                  <span><br></span><input type="radio" name="startingCoordinate" value="m11" />
                </td>
                <td class="blank" id="n11">
                  <span><br></span><input type="radio" name="startingCoordinate" value="n11" />
                </td>
                <td class="blank" id="o11">
                  <span><br></span><input type="radio" name="startingCoordinate" value="o11" />
                </td>
              </tr>
              <tr>
                <td class="double-letter" id="a12">
                  <span><br></span><input type="radio" name="startingCoordinate" value="a12" />
                </td>
                <td class="blank" id="b12">
                  <span><br></span><input type="radio" name="startingCoordinate" value="b12" />
                </td>
                <td class="blank" id="c12">
                  <span><br></span><input type="radio" name="startingCoordinate" value="c12" />
                </td>
                <td class="double-word" id="d12">
                  <span><br></span><input type="radio" name="startingCoordinate" value="d12" />
                </td>
                <td class="blank" id="e12">
                  <span><br></span><input type="radio" name="startingCoordinate" value="e12" />
                </td>
                <td class="blank" id="f12">
                  <span><br></span><input type="radio" name="startingCoordinate" value="f12" />
                </td>
                <td class="blank" id="g12">
                  <span><br></span><input type="radio" name="startingCoordinate" value="g12" />
                </td>
                <td class="double-letter" id="h12">
                  <span><br></span><input type="radio" name="startingCoordinate" value="h12" />
                </td>
                <td class="blank" id="i12">
                  <span><br></span><input type="radio" name="startingCoordinate" value="i12" />
                </td>
                <td class="blank" id="j12">
                  <span><br></span><input type="radio" name="startingCoordinate" value="j12" />
                </td>
                <td class="blank" id="k12">
                  <span><br></span><input type="radio" name="startingCoordinate" value="k12" />
                </td>
                <td class="double-word" id="l12">
                  <span><br></span><input type="radio" name="startingCoordinate" value="l12" />
                </td>
                <td class="blank" id="m12">
                  <span><br></span><input type="radio" name="startingCoordinate" value="m12" />
                </td>
                <td class="blank" id="n12">
                  <span><br></span><input type="radio" name="startingCoordinate" value="n12" />
                </td>
                <td class="double-letter" id="o12">
                  <span><br></span><input type="radio" name="startingCoordinate" value="o12" />
                </td>
              </tr>
              <tr>
                <td class="blank" id="a13">
                  <span><br></span><input type="radio" name="startingCoordinate" value="a13" />
                </td>
                <td class="blank" id="b13">
                  <span><br></span><input type="radio" name="startingCoordinate" value="b13" />
                </td>
                <td class="double-word" id="c13">
                  <span><br></span><input type="radio" name="startingCoordinate" value="c13" />
                </td>
                <td class="blank" id="d13">
                  <span><br></span><input type="radio" name="startingCoordinate" value="d13" />
                </td>
                <td class="blank" id="e13">
                  <span><br></span><input type="radio" name="startingCoordinate" value="e13" />
                </td>
                <td class="blank" id="f13">
                  <span><br></span><input type="radio" name="startingCoordinate" value="f13" />
                </td>
                <td class="double-letter" id="g13">
                  <span><br></span><input type="radio" name="startingCoordinate" value="g13" />
                </td>
                <td class="blank" id="h13">
                  <span><br></span><input type="radio" name="startingCoordinate" value="h13" />
                </td>
                <td class="double-letter" id="i13">
                  <span><br></span><input type="radio" name="startingCoordinate" value="i13" />
                </td>
                <td class="blank" id="j13">
                  <span><br></span><input type="radio" name="startingCoordinate" value="j13" />
                </td>
                <td class="blank" id="k13">
                  <span><br></span><input type="radio" name="startingCoordinate" value="k13" />
                </td>
                <td class="blank" id="l13">
                  <span><br></span><input type="radio" name="startingCoordinate" value="l13" />
                </td>
                <td class="double-word" id="m13">
                  <span><br></span><input type="radio" name="startingCoordinate" value="m13" />
                </td>
                <td class="blank" id="n13">
                  <span><br></span><input type="radio" name="startingCoordinate" value="n13" />
                </td>
                <td class="blank" id="o13">
                  <span><br></span><input type="radio" name="startingCoordinate" value="o13" />
                </td>
              </tr>
              <tr>
                <td class="blank" id="a14">
                  <span><br></span><input type="radio" name="startingCoordinate" value="a14" />
                </td>
                <td class="double-word" id="b14">
                  <span><br></span><input type="radio" name="startingCoordinate" value="b14" />
                </td>
                <td class="blank" id="c14">
                  <span><br></span><input type="radio" name="startingCoordinate" value="c14" />
                </td>
                <td class="blank" id="d14">
                  <span><br></span><input type="radio" name="startingCoordinate" value="d14" />
                </td>
                <td class="blank" id="e14">
                  <span><br></span><input type="radio" name="startingCoordinate" value="e14" />
                </td>
                <td class="triple-letter" id="f14">
                  <span><br></span><input type="radio" name="startingCoordinate" value="f14" />
                </td>
                <td class="blank" id="g14">
                  <span><br></span><input type="radio" name="startingCoordinate" value="g14" />
                </td>
                <td class="blank" id="h14">
                  <span><br></span><input type="radio" name="startingCoordinate" value="h14" />
                </td>
                <td class="blank" id="i14">
                  <span><br></span><input type="radio" name="startingCoordinate" value="i14" />
                </td>
                <td class="triple-letter" id="j14">
                  <span><br></span><input type="radio" name="startingCoordinate" value="j14" />
                </td>
                <td class="blank" id="k14">
                  <span><br></span><input type="radio" name="startingCoordinate" value="k14" />
                </td>
                <td class="blank" id="l14">
                  <span><br></span><input type="radio" name="startingCoordinate" value="l14" />
                </td>
                <td class="blank" id="m14">
                  <span><br></span><input type="radio" name="startingCoordinate" value="m14" />
                </td>
                <td class="double-word" id="n14">
                  <span><br></span><input type="radio" name="startingCoordinate" value="n14" />
                </td>
                <td class="blank" id="o14">
                  <span><br></span><input type="radio" name="startingCoordinate" value="o14" />
                </td>
              </tr>
              <tr>
                <td class="triple-word" id="a15">
                  <span><br></span><input type="radio" name="startingCoordinate" value="a15" />
                </td>
                <td class="blank" id="b15">
                  <span><br></span><input type="radio" name="startingCoordinate" value="b15" />
                </td>
                <td class="blank" id="c15">
                  <span><br></span><input type="radio" name="startingCoordinate" value="c15" />
                </td>
                <td class="double-letter" id="d15">
                  <span><br></span><input type="radio" name="startingCoordinate" value="d15" />
                </td>
                <td class="blank" id="e15">
                  <span><br></span><input type="radio" name="startingCoordinate" value="e15" />
                </td>
                <td class="blank" id="f15">
                  <span><br></span><input type="radio" name="startingCoordinate" value="f15" />
                </td>
                <td class="blank" id="g15">
                  <span><br></span><input type="radio" name="startingCoordinate" value="g15" />
                </td>
                <td class="triple-word" id="h15">
                  <span><br></span><input type="radio" name="startingCoordinate" value="h15" />
                </td>
                <td class="blank" id="i15">
                  <span><br></span><input type="radio" name="startingCoordinate" value="i15" />
                </td>
                <td class="blank" id="j15">
                  <span><br></span><input type="radio" name="startingCoordinate" value="j15" />
                </td>
                <td class="blank" id="k15">
                  <span><br></span><input type="radio" name="startingCoordinate" value="k15" />
                </td>
                <td class="double-letter" id="l15">
                  <span><br></span><input type="radio" name="startingCoordinate" value="l15" />
                </td>
                <td class="blank" id="m15">
                  <span><br></span><input type="radio" name="startingCoordinate" value="m15" />
                </td>
                <td class="blank" id="n15">
                  <span><br></span><input type="radio" name="startingCoordinate" value="n15" />
                </td>
                <td class="triple-word" id="o15">
                  <span><br></span><input type="radio" name="startingCoordinate" value="o15" />
                </td>
              </tr>
            </table>
          </div>
        </div>
    </div>
    <!--/Scrabble Board-->


</div><!--/.container-->

  <footer class="footer"> <!--Sticky footer-->
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-xs-offset-4">
          <h3 class="text-uppercase">
              <s:iterator value="playerView.myTiles" var="item">
                <s:property value="#item" />
              </s:iterator>
          </h3>
        </div>
      </div>
      <div class="row">
          <div class="col-xs-8">
            <div class="form-group">
              <label for="word">Your word</label>
              <input type="text" class="form-control" id="word" name="word" />
            </div>
          </div>
          <div class="col-xs-4">
            <div class="checkbox">
              <label>
                <input type="checkbox" value="pass" name="pass" />Pass your turn
              </label>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="radio">
              <s:if test="%{playerView.isGameOver}">
                <label>
                  <input type="radio" name="direction" id="direction-horizontal" value="horizontal" disabled required />
                  Horizontal
                </label>
                <label>
                  <input type="radio" name="direction" id="direction-vertical" value="vertical" disabled />
                  Vertical
                </label>
              </s:if>
              <s:else>
                <label>
                  <input type="radio" name="direction" id="direction-horizontal" value="horizontal" />
                  Horizontal
                </label>
                <label>
                  <input type="radio" name="direction" id="direction-vertical" value="vertical" />
                  Vertical
                </label>
              </s:else>
            </div>
          </div>
          <div class="col-xs-12">
            <s:if test="%{playerView.isMyTurn && playerView.isGameStarted && !playerView.isGameOver}">
              <button type="submit" class="btn btn-default">Submit</button>
            </s:if>
            <s:else>
              <button type="submit" class="btn btn-default" disabled="disabled">Submit</button>
            </s:else>
          </div>
      </div>
    </div>
  </footer><!--/Sticky footer-->
</form>
<%@ include file="footer.jsp"%>
