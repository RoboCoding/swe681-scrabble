function addLetter(a, b, letter){
    var x = parseInt(a).toString();
    var y = parseInt(b).toString();

    switch(x){
        case "1":
            x = "a";
            break;
        case "2":
            x = "b";
            break;
        case "3":
            x = "c";
            break;
        case "4":
            x = "d";
            break;
        case "5":
            x = "e";
            break;
        case "6":
            x = "f";
            break;
        case "7":
            x = "g";
            break;
        case "8":
            x = "h";
            break;
        case "9":
            x = "i";
            break;
        case "10":
            x = "j";
            break;
        case "11":
            x = "k";
            break;
        case "12":
            x = "l";
            break;
        case "13":
            x = "m";
            break;
        case "14":
            x = "n";
            break;
        case "15":
            x = "o";
            break;
    }

    $("#"+x+y).removeClass(function(){
        return "triple-word triple-letter double-word double-letter blank star";
    }).addClass("occupied");

    $("#"+x+y+" span").prepend(letter.toString());
}


